using UnityEngine;

namespace Testing
{
    public class TestFollowCamera : MonoBehaviour
    {
        [SerializeField] private Vector3 offset = new Vector3(0, 5, -10);
        [SerializeField] private Transform target;
        [SerializeField] private float translationSmoothTime = 10f;
        [SerializeField] private float rotationSpeed = 10f;
        [SerializeField] private Camera camera;

        private Vector3 _translationVelocity = Vector3.zero;
        private Transform _cameraTransform;
        private bool initialized;

        private void OnEnable()
        {
            if (GameManager.CurrentGameState == GameManager.GameState.Playing)
            {
                Init();
                return;
            }

            GameManager.OnGameStateChanged += state =>
            {
                if (state == GameManager.GameState.Playing) Init();
            };
        }

        public void Init()
        {
            Utils.CustomDebugLog(Utils.LogLevel.Info, "TestFollowCamera initializing");
            if (camera == null)
            {
                Utils.CustomDebugLog(Utils.LogLevel.Info, "Camera not set, assuming main camera");
                camera = Camera.main;
            }

            Utils.CustomDebugLog(Utils.LogLevel.Info, $"TestFollowCamera camera is {camera}");

            Vector3 pos = target.TransformPoint(offset);
            _cameraTransform = camera.transform;
            _cameraTransform.position = pos;
            _cameraTransform.LookAt(target);

            initialized = true;
        }

        private void FixedUpdate()
        {
            if (!initialized) return;

            // Translate
            Vector3 pos = target.TransformPoint(offset);
            _cameraTransform.position = Vector3.SmoothDamp(_cameraTransform.position, pos,
                ref _translationVelocity, translationSmoothTime);
            // transform.position = pos;

            // Rotate
            Vector3 dir = target.position - _cameraTransform.position;
            Quaternion lookRot = Quaternion.LookRotation(dir, Vector3.up);
            _cameraTransform.rotation = Quaternion.RotateTowards(_cameraTransform.rotation, lookRot,
                rotationSpeed * Time.deltaTime);
            // transform.rotation = Quaternion.Lerp(transform.rotation, lookRot, rotationSpeed * Time.deltaTime);
        }
    }
}