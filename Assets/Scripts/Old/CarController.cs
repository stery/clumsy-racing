﻿using UnityEngine;

public class CarController : MonoBehaviour
{
    [SerializeField] private WheelCollider leftFrontCollider;
    [SerializeField] private WheelCollider rightFrontCollider;
    [SerializeField] private WheelCollider leftRearCollider;
    [SerializeField] private WheelCollider rightRearCollider;
    [SerializeField] private float motorTorque;
    [SerializeField] private float maxSteeringAngle;

    private float _forwardMovement;
    private float _steering;

    public float ForwardMovement
    {
        get => _forwardMovement;
        set => _forwardMovement = Mathf.Clamp(value, -1f, 1f);
    }
    public float Steering
    {
        get => _steering;
        set => _steering = Mathf.Clamp(value, -1f, 1f);
    }

    private void FixedUpdate()
    {
        ApplyForwardMovement(ForwardMovement);
        ApplySteering(Steering);
    }

    private void ApplyForwardMovement(float vertical)
    {
        leftFrontCollider.motorTorque = motorTorque * vertical;
        rightFrontCollider.motorTorque = motorTorque * vertical;
        // TODO: Braking?
    }

    private void ApplySteering(float horizontal)
    {
        leftFrontCollider.steerAngle = maxSteeringAngle * horizontal;
        rightFrontCollider.steerAngle = maxSteeringAngle * horizontal;
    }
}