using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CarControllerOld : MonoBehaviour
{
    [SerializeField] private float maxSpeed = 5f;
    [SerializeField] private float acceleration = 1f;
    [SerializeField] private float drag = 0.2f;
    [SerializeField] private float steeringForce = 1f;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.maxAngularVelocity = 3f;
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Move(vertical);
        Steer(horizontal);
    }

    private void Steer(float horizontal)
    {
        /*
         * TODO: Ground check, don't spin in air,
         * or at least spin on Vector.up (not transform.up) axis
         * TODO: Decrease steering force w/ higher speed
         * TODO: Don't spin in-place (0 speed), spin slowly at low speeds (barely moving)
         */
        float forwardSpeed = _rigidbody.velocity.z;
        if (forwardSpeed < 0.01f)
        {
            return;
        }
        float torque = horizontal * steeringForce * 1000 * Time.fixedDeltaTime;
        // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Adding torque: {torque} to {transform.right}");
        
        _rigidbody.AddTorque(transform.up * torque);
    }

    private void Move(float vertical)
    {
        if (Mathf.Abs(vertical) < 0.01f)
        {
            return;
        }

        float forwardSpeed = _rigidbody.velocity.z;
        // TODO: Fiddle with fixedDeltaTime compensation (100), increased from 10 after adding deltaTime without testing
        float force = vertical * acceleration * 1000 * Time.fixedDeltaTime;

        if (Mathf.Abs(forwardSpeed) > maxSpeed && Math.Abs(Mathf.Sign(forwardSpeed) - Mathf.Sign(force)) < 0.01f)
        {
            force = 0f;
        }
        
        Utils.CustomDebugLog(Utils.LogLevel.Info, $"Adding force: {force} to {transform.forward}");

        _rigidbody.AddForce(transform.forward * force);
    }
}