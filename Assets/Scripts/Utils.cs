﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public static class Utils
{
    public static List<Vector3> GetHermitePoints(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, int count)
    {
        if (count < 2)
        {
            throw new InvalidOperationException("Can't generate less than two points");
        }

        // the points are between [p1, p2].
        Vector3 m1 = (p2 - p0) / 2;
        Vector3 m2 = (p3 - p1) / 2;

        List<Vector3> points = new List<Vector3>();

        float step = 1f / (count - 1); // count-1 because there will be count+1 points otherwise

        for (float t = 0; t <= 1 + 0.01f; t += step)
        {
            float t2 = t * t;
            float t3 = t2 * t;
            Vector3 point = (2 * t3 - 3 * t2 + 1) * p1
                            + (t3 - 2 * t2 + t) * m1
                            + (-2 * t3 + 3 * t2) * p2
                            + (t3 - t2) * m2;
            points.Add(point);
        }

        return points;
    }

    public static List<float> RandomWalk(int count, float delta)
    {
        List<float> result = new List<float>(count);
        float value = 0f;
        int sign;

        for (int i = 0; i < count; ++i)
        {
            sign = Random.Range(-1, 2);
            value += sign * delta;
            result.Add(value);
        }

        return result;
    }

    public static List<Vector3> HermiteWithRandomWalkHeight(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3,
                                                            int hermiteCount, int randomWalkCount,
                                                            float randomWalkDelta)
    {
        if (hermiteCount == 0 || randomWalkCount == 0)
        {
            string errMsg = "Can't use Hermite and Random Walk with one of the counts 0 " +
                            $"(hermite: {hermiteCount}, randomWalk: {randomWalkCount})";
            Utils.CustomDebugLog(Utils.LogLevel.Error, errMsg);
            throw new InvalidOperationException(errMsg);
        }

        // ===== Compute initial points =====
        // +2 because first and last are included and will not be modified by Random Walk
        int initialPointsCount = randomWalkCount + 2;
        List<Vector3> initialPoints = GetHermitePoints(p0, p1, p2, p3, initialPointsCount);

        if (initialPoints.Count != initialPointsCount)
        {
            int actualCount = initialPoints.Count;
            initialPoints = initialPoints.Take(initialPointsCount - 1)
                .Skip(actualCount - initialPointsCount - 2)
                .ToList();
            Utils.CustomDebugLog(Utils.LogLevel.Warn, $"Initial points size: {actualCount} / " +
                             $"required: {initialPointsCount} / " +
                             $"new size: {initialPoints.Count}");
        }

        // ===== Random Walk for initial points =====
        List<float> heightDeltas = RandomWalk(randomWalkCount, randomWalkDelta);
        float minHeight = Math.Min(p1.y, p2.y);

        for (int i = 0; i < randomWalkCount; ++i)
        {
            Vector3 point = initialPoints[i + 1];
            float height = point.y + heightDeltas[i];
            point.y = Math.Max(height, minHeight); // Clamp to lowest of p1 and p2
            initialPoints[i + 1] = point;
        }

        // ===== Prepare points used by Hermite =====
        /*
         * `initialPoints` is a spline between (p1, p2).
         * In order to also keep p1 and p2 when applying Hermite again
         * (after the height change from Random Walk),
         * two more points have to be used (for tangents),
         * but p0 and p3 can't be safely used because
         * the distance to them is usually way higher
         * than the distance between points in `initialPoints`.
         *
         * Therefore, h0 and h3 are "artificially" created
         * by mirroring the second first and second last points
         * with respect to the first and the last point,
         * in order to keep similar distances between all points.
         */
        List<Vector3> hermiteHelpingPoints = new List<Vector3>();

        Vector3 h0 = 2 * initialPoints[0] - initialPoints[1]; // i0 - (i1 - i0 (= i0's forward))
        Vector3 h3 = 2 * initialPoints[initialPointsCount - 1] -
                     initialPoints[initialPointsCount - 2]; // iN + (iN - iN_-1 (= iN_-1's forward))

        hermiteHelpingPoints.Add(h0);
        hermiteHelpingPoints.AddRange(initialPoints);
        hermiteHelpingPoints.Add(h3);

        int helpingPointsCount = hermiteHelpingPoints.Count;

        Utils.CustomDebugLog(Utils.LogLevel.Info, $"h0: {hermiteHelpingPoints[0]} | p0: {p0}\n" +
                  $"h1: {hermiteHelpingPoints[1]} | p1: {p1}\n" +
                  $"h2: {hermiteHelpingPoints[helpingPointsCount - 2]} | p2: {p2}\n" +
                  $"h3: {hermiteHelpingPoints[helpingPointsCount - 1]} | p3: {p3}");

        // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Helping points: {ListToString(hermiteHelpingPoints)}");

        // ===== Prepare and compute Hermite chunks =====
        // -2 because first and last are used as tangents, -1 because they are segments between points
        int chunksCount = helpingPointsCount - 3;
        // +1 because last point will not be added (except for the last iteration)
        int chunkSize = (hermiteCount / chunksCount) + 1;
        List<Vector3> points = new List<Vector3>();

        for (int point = 1; point < helpingPointsCount - 2; ++point)
        {
            Vector3 p0t = hermiteHelpingPoints[point - 1];
            Vector3 p1t = hermiteHelpingPoints[point];
            Vector3 p2t = hermiteHelpingPoints[point + 1];
            Vector3 p3t = hermiteHelpingPoints[point + 2];

            List<Vector3> chunkPoints = GetHermitePoints(p0t, p1t, p2t, p3t, chunkSize);
            // Clamp height above minHeight again
            for (var index = 0; index < chunkPoints.Count; index++)
            {
                Vector3 chunkPoint = chunkPoints[index];
                chunkPoint.y = Math.Max(chunkPoint.y, minHeight);
                chunkPoints[index] = chunkPoint;
            }

            points.AddRange(chunkPoints.Take(chunkSize - 1)); // Avoid duplicating now's p2t and next's p1t
        }

        // Add the last point which was not added by prevention above
        points.Add(p2);

        // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Returning {points.Count} points: {ListToString(points)}");

        return points;
    }

    public static List<Vector3> BlendPointsEnds(List<Vector3> points1, List<Vector3> points2, int count)
    {
        if (points1.Count < count || points2.Count < count)
        {
            throw new InvalidOperationException("count is larger than provided points");
        }

        List<Vector3> result = points2.ToList();

        float factor = 1f;
        for (int i = 0; i < count; ++i)
        {
            result[i] = points1[i] * factor + points2[i] * (1 - factor);
            result[result.Count - 1 - i] =
                points1[points1.Count - 1 - i] * factor +
                points2[points2.Count - 1 - i] * (1 - factor);
            factor -= 1f / count;
        }

        return result;
    }

    /// <summary>
    /// The function takes a list of <b>points</b> from which are selected
    /// <b>peaksCount</b> points which represent the "<i>peaks</i>".
    /// <br/>
    /// These <i>peaks</i>' heights are then changed by <b>deltaUp</b> steps
    /// using the Random Walk algorithm and are then interpolated
    /// using <see cref="GetHermitePoints"/>.
    /// <br/>
    /// If <b>keepStartEndSame</b> is true, the first and last points are not modified.
    /// </summary>
    /// <param name="points">The list of points to have their heights randomized</param>
    /// <param name="peaksCount">Number of points to be considered as peaks. Must be less than points size.</param>
    /// <param name="deltaUp">The step value used to change the peaks' heights</param>
    /// <param name="keepStartEndSame">The flag used to preserve the first and last points, if true</param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException">Thrown when <b>peaksCount</b> is less than <b>points</b>'s size</exception>
    [Obsolete]
    public static List<Vector3> RandomWalkHermiteNew(List<Vector3> points, int peaksCount, float deltaUp,
                                                     bool keepStartEndSame = true)
    {
        if (peaksCount > points.Count - 4)
        {
            if (keepStartEndSame)
            {
                string errMsg =
                    $"Peaks count should be at most points.Count - 4 ({points.Count} - 4) when using keepStartEndSame true";
                Utils.CustomDebugLog(Utils.LogLevel.Error, errMsg);
                throw new InvalidOperationException(errMsg);
            }

            if (peaksCount > points.Count)
            {
                string errMsg = $"Peaks count should be at most points.Count ({points.Count})";
                Utils.CustomDebugLog(Utils.LogLevel.Error, errMsg);
                throw new InvalidOperationException(errMsg);
            }
        }

        // TODO: There seems to be a gap between walls to the ending if using this one
        // But this method is safer (previous version throws out of range for too low peaksCount
        List<Vector3> result = new List<Vector3>();

        float currentDeltaUp = 0f;
        int peaksIntervalSize = points.Count / peaksCount;
        // A peak is represented by a tuple like (id_of_point_representing_this_peak, deltaUp)
        Tuple<int, float>[] peaks = new Tuple<int, float>[keepStartEndSame ? peaksCount + 4 : peaksCount];
        int startIndex = 0, endIndex = peaksCount - 1;
        if (keepStartEndSame)
        {
            peaks[startIndex++] = new Tuple<int, float>(0, 0f);
            peaks[startIndex++] = new Tuple<int, float>(1, 0f);
            peaks[endIndex + 2] = new Tuple<int, float>(points.Count - 1, 0f);
            peaks[endIndex + 1] = new Tuple<int, float>(points.Count - 2, 0f);
            // TODO: Add 2nd first and 2nd last?
            result.AddRange(points.Take(2));
        }

        // for (int i = startIndex * peaksIntervalSize; i < (peaksCount - 1) * peaksIntervalSize; i += peaksIntervalSize)
        // {
        //     
        // }

        for (int i = startIndex; i <= endIndex; i++)
        {
            int direction = Random.Range(-1, 2);
            switch (direction)
            {
                case -1: // Go down
                    currentDeltaUp -= deltaUp; // TODO: Prevent going lower than the next control point
                    break;
                case 1: // Go up
                    currentDeltaUp += deltaUp;
                    break;
                case 0: // Keep level
                default:
                    break;
            }

            // peaks[i] = currentDeltaUp;
            peaks[i] = new Tuple<int, float>(i * peaksIntervalSize, currentDeltaUp);
        }

        for (int i = startIndex; i <= endIndex; i++)
        {
            Tuple<int, float> peak0 = peaks[Mathf.Max(0, i - 1)];
            Tuple<int, float> peak1 = peaks[i];
            Tuple<int, float> peak2 = peaks[i + 1];
            Tuple<int, float> peak3 = peaks[Mathf.Min(endIndex, i + 2)];
            Vector3 p0 = points[peak0.Item1] + Vector3.up * peak0.Item2;
            Vector3 p1 = points[peak1.Item1] + Vector3.up * peak1.Item2;
            Vector3 p2 = points[peak2.Item1] + Vector3.up * peak2.Item2;
            Vector3 p3 = points[peak3.Item1] + Vector3.up * peak3.Item2;
            List<Vector3> hermite = GetHermitePoints(p0, p1, p2, p3, peaksIntervalSize);
            result.AddRange(hermite.Take(hermite.Count - 1));
        }

        if (keepStartEndSame)
        {
            result.AddRange(points.Skip(points.Count - 2));
        }

        return result;

        //
        // result.AddRange(points.Take(peaksToPoints));
        // for (int i = 1; i < peaksCount - 2; i++)
        // {
        //     Vector3 p0 = points[(i - 1) * peaksToPoints] + Vector3.up * peaks[i - 1];
        //     Vector3 p1 = points[i * peaksToPoints] + Vector3.up * peaks[i];
        //     Vector3 p2 = points[(i + 1) * peaksToPoints] + Vector3.up * peaks[i + 1];
        //     Vector3 p3 = points[(i + 2) * peaksToPoints - 1] + Vector3.up * peaks[i + 2];
        //     List<Vector3> hermite = GetHermitePoints(p0, p1, p2, p3, peaksToPoints);
        //     result.AddRange(hermite.Take(hermite.Count - 1));
        // }
        //
        // result.AddRange(points.Skip(points.Count - peaksToPoints));
        // return result;
    }

    public static float Euler360AngleToNegativePositive180(float angle)
    {
        return angle <= 180f ? angle : angle - 360f;
    }

    public static float Rescale(float value, float fromMin, float fromMax, float toMin, float toMax)
    {
        // TODO: Test this
        return (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
    }

    public static bool IsPointOverUIObject(Vector2 pos)
    {
        // if (EventSystem.current.IsPointerOverGameObject())
        //     return false;

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = pos
        };
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public static string PrettyHierarchyString(Transform transform, int level = 0)
    {
        string result = new string('\t', level) +
                        $"Name: {transform.name}, WorldPos: {transform.position}, LocalPos: {transform.localPosition}\n";

        foreach (Transform child in transform)
        {
            result += PrettyHierarchyString(child, level + 1);
        }

        return result;
    }

    public static string ListToString<T>(List<T> list)
    {
        return "[" + String.Join(", ", list) + "]";
    }

    public static Vector3[] DeepCopyVector3s(Vector3[] coords)
    {
        Vector3[] result = new Vector3[coords.Length];
        for (int i = 0; i < coords.Length; i++)
        {
            result[i] = new Vector3(coords[i].x, coords[i].y, coords[i].z);
        }

        return result;
    }

    public static Vector3 GetLeftVector(Vector3 forward, Vector3 up)
    {
        return Vector3.Cross(up, forward).normalized;
    }

    public static Bounds GetComposedBounds(Transform transform)
    {
        MeshRenderer[] meshRenderers = transform.GetComponentsInChildren<MeshRenderer>();
        Bounds bounds = meshRenderers[0].bounds;
        Vector3 min = bounds.min;
        Vector3 max = bounds.max;

        for (int i = 1; i < meshRenderers.Length; ++i)
        {
            Bounds meshRendererBounds = meshRenderers[i].bounds;
            Vector3 currentMin = meshRendererBounds.min;
            Vector3 currentMax = meshRendererBounds.max;

            if (currentMin.x < min.x)
            {
                min.x = currentMin.x;
            }

            if (currentMin.y < min.y)
            {
                min.y = currentMin.y;
            }

            if (currentMin.z < min.z)
            {
                min.z = currentMin.z;
            }

            if (currentMax.x > max.x)
            {
                max.x = currentMax.x;
            }

            if (currentMax.y > max.y)
            {
                max.y = currentMax.y;
            }

            if (currentMax.z > max.z)
            {
                max.z = currentMax.z;
            }
        }

        bounds.SetMinMax(min, max);

        return bounds;
    }

    public static float[] ComputeDistanceBetweenPathPoints(Vector3[] pathPoints)
    {
        int count = pathPoints.Length;
        float[] distanceBetweenPoints = new float[count];

        for (int i = 1; i < count; ++i)
        {
            distanceBetweenPoints[i] = Vector3.Distance(pathPoints[i], pathPoints[i - 1]);
        }

        return distanceBetweenPoints;
    }

    public static Color GetRandomColor()
    {
        Color[] colors = {Color.blue, Color.cyan, Color.green, Color.magenta, Color.red, Color.white, Color.yellow};
        return colors[Random.Range(0, colors.Length)];
    }

    public static Vector3 GetClosestPoint(List<Vector3> points, Vector3 position)
    {
        Vector3 minPoint = points[0];
        float minDistance = Single.MaxValue;

        points.ForEach(point =>
        {
            float distance = Vector3.Distance(point, position);
            if (distance < minDistance)
            {
                minPoint = point;
                minDistance = distance;
            }
        });

        return minPoint;
    }

    public static Vector3[] GetBlendedHermiteWithRandomWalkHeight(Vector3[] points,
                                                                  int hermiteDivisionsCount, int randomWalkPeaksCount,
                                                                  float randomWalkDelta, int count)
    {
        List<Vector3> hermitePoints = GetHermitePoints(points[0], points[1], points[2], points[3],
            hermiteDivisionsCount);
        List<Vector3> randomWalkedHermitePoints = HermiteWithRandomWalkHeight(points[0], points[1], points[2], points[3],
            hermiteDivisionsCount, randomWalkPeaksCount, randomWalkDelta);

        return BlendPointsEnds(hermitePoints, randomWalkedHermitePoints, count).ToArray();
    }

    public static void CustomDebugLog(LogLevel logLevel, string message)
    {
        if (!Debug.isDebugBuild || !DependenciesManager.GameConfiguration.EnableDebugLogs)
        {
            return;
        }

        switch (logLevel)
        {
            case LogLevel.Warn:
                Debug.LogWarning(message);
                break;
            case LogLevel.Error:
                Debug.LogError(message);
                break;
            case LogLevel.Info:
            default:
                Debug.Log(message);
                break;
        }
    }

    public enum LogLevel
    {
        Info,
        Warn,
        Error
    }
}