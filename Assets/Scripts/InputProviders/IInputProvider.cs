﻿using System;
using UnityEngine;

namespace InputProviders
{
    public interface IInputProvider
    {
        public event Action<Vector2> OnClickEvent;
        public float GetForwardInput();
        public float GetSteeringInput();
        public void Destroy();
    }
}