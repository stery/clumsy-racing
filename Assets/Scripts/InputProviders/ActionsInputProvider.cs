﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace InputProviders
{
    public class ActionsInputProvider : IInputProvider, DefaultPlayerInput.IGameStateActions
    {
        public event Action<Vector2> OnClickEvent;
        
        private readonly DefaultPlayerInput _playerInput;
        private readonly InputAction _moveAction;
        private readonly InputAction _steerAction;
        private readonly DefaultPlayerInput.CursorActions _cursorActions;
        private readonly InputAction _cursorPosition;

        public ActionsInputProvider(DefaultPlayerInput defaultPlayerInput)
        {
            Utils.CustomDebugLog(Utils.LogLevel.Info, "ActionsInputProvider created");
            _playerInput = defaultPlayerInput;
            _playerInput.Enable();
            _moveAction = _playerInput.Player.Move;
            _steerAction = _playerInput.Player.Steer;
            _cursorActions = _playerInput.Cursor;
            _cursorPosition = _cursorActions.Position;
            
            _cursorActions.Click.performed += OnClick;

            _playerInput.GameState.SetCallbacks(this);
        }

        ~ActionsInputProvider()
        {
            Destroy();
        }

        public void Destroy()
        {
            _cursorActions.Click.performed -= OnClick;
            _playerInput.Disable();
            OnClickEvent = null;
        }

        public float GetForwardInput()
        {
            return _moveAction.ReadValue<float>();
        }

        public float GetSteeringInput()
        {
            return _steerAction.ReadValue<float>();
        }

        // TODO: Should be decoupled
        private void ChangeGameState(GameManager.GameState targetState)
        {
            GameManager.Instance.ChangeGameState(targetState);
        }

        public void OnEdit(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                ChangeGameState(GameManager.GameState.EditingRoad);
            }
        }

        public void OnPlay(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Utils.CustomDebugLog(Utils.LogLevel.Info, "Play pressed");
                ChangeGameState(GameManager.GameState.Playing);
            }
        }

        public void OnExit(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Utils.CustomDebugLog(Utils.LogLevel.Info, "Exit pressed");
                GameManager.Instance.Quit();
            }
        }

        public void OnRestart(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Utils.CustomDebugLog(Utils.LogLevel.Info, "Restart pressed");
                GameManager.Instance.Restart();
            }
        }

        public void OnOcclusion(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                GameManager.Instance.ToggleOcclusion();
            }
        }

        private void OnClick(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Vector2 cursorPosition = _cursorPosition.ReadValue<Vector2>();
                Utils.CustomDebugLog(Utils.LogLevel.Info, $"OnClick, mouse position at {cursorPosition}");

                OnClickEvent?.Invoke(cursorPosition);
            }
        }
    }
}