﻿using System;
using UI;
using UnityEngine;

namespace InputProviders
{
    public class GUIInputProvider : IInputProvider
    {
        public event Action<Vector2> OnClickEvent;

        private float _forward, _steering;

        public GUIInputProvider()
        {
            WrappedUIButton.OnButtonEvent += OnUIButtonEvent;
        }

        ~GUIInputProvider()
        {
            Destroy();
        }

        public void Destroy()
        {
            WrappedUIButton.OnButtonEvent -= OnUIButtonEvent;
            OnClickEvent = null;
        }

        public float GetForwardInput()
        {
            return _forward;
        }

        public float GetSteeringInput()
        {
            return _steering;
        }

        private void OnUIButtonEvent(UIButtonEventData eventData)
        {
            UIButtonActionType actionType = eventData.ActionType;
            UIButtonEventType eventType = eventData.EventType;

            switch (actionType)
            {
                case UIButtonActionType.MoveForward:
                    _forward = GetValueFromEventType(eventType);
                    break;
                case UIButtonActionType.MoveBackward:
                    _forward = -GetValueFromEventType(eventType);
                    break;
                case UIButtonActionType.SteerLeft:
                    _steering = -GetValueFromEventType(eventType);
                    break;
                case UIButtonActionType.SteerRight:
                    _steering = GetValueFromEventType(eventType);
                    break;
                default:
                    throw new InvalidOperationException("Unknown action type");
            }
            
            OnClickEvent?.Invoke(eventData.Position);
        }

        private float GetValueFromEventType(UIButtonEventType eventType)
        {
            switch (eventType)
            {
                case UIButtonEventType.Click:
                case UIButtonEventType.Down:
                    return 1f;
                case UIButtonEventType.Up:
                default:
                    return 0f;
            }
        }
    }
}