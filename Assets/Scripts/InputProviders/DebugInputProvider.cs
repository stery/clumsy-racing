﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace InputProviders
{
    public class DebugInputProvider : IInputProvider, IPointerClickHandler
    {
        public event Action<Vector2> OnClickEvent;

        ~DebugInputProvider()
        {
            Destroy();
        }

        public void Destroy()
        {
            OnClickEvent = null;
        }

        public float GetForwardInput()
        {
            return Input.GetAxis("Vertical");
        }

        public float GetSteeringInput()
        {
            return Input.GetAxis("Horizontal");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClickEvent?.Invoke(eventData.position);
        }
    }
}