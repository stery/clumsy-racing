﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

public readonly struct Road
{
    public ARCube From { get; }
    public ARCube To { get; }
    public GameObject RoadGameObject { get; }
    public ARAnchor ARAnchor { get; }

    public Road(ARCube from, ARCube to, GameObject roadGameObject, ARAnchor arAnchor)
    {
        From = from;
        To = to;
        RoadGameObject = roadGameObject;
        ARAnchor = arAnchor;
    }

    public void Destroy(bool destroyAnchor = false)
    {
        Object.Destroy(RoadGameObject);
        // if (destroyAnchor)
        // {
        //     Object.Destroy(ARAnchor.gameObject);
        // }
    }
}