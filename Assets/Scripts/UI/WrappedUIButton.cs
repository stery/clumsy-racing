﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class WrappedUIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        [SerializeField] private UIButtonActionType actionType;

        public delegate void OnButtonEventHandler(UIButtonEventData eventData);

        public static event OnButtonEventHandler OnButtonEvent;

        private void Awake()
        {
            if (actionType == null)
            {
                throw new InvalidOperationException("Button action type is not set");
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            UIButtonEventData buttonEventData = new UIButtonEventData
                {ActionType = actionType, EventType = UIButtonEventType.Down, Position = eventData.position};

            OnButtonEvent?.Invoke(buttonEventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            UIButtonEventData buttonEventData = new UIButtonEventData
                {ActionType = actionType, EventType = UIButtonEventType.Up, Position = eventData.position};

            OnButtonEvent?.Invoke(buttonEventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            UIButtonEventData buttonEventData = new UIButtonEventData
                {ActionType = actionType, EventType = UIButtonEventType.Click, Position = eventData.position};

            OnButtonEvent?.Invoke(buttonEventData);
        }
    }
}