﻿using UnityEngine;

namespace UI
{
    public struct UIButtonEventData
    {
        public UIButtonActionType ActionType;
        public UIButtonEventType EventType;
        public Vector2 Position;
    }

    public enum UIButtonActionType
    {
        MoveForward, MoveBackward, SteerLeft, SteerRight
    }

    public enum UIButtonEventType
    {
        Click, Down, Up
    }
}