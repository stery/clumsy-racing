﻿using System.Collections.Generic;
using System.Linq;
using Meshes;
using UnityEngine;
using UnityEngine.AI;

public class RoadsManager : MonoBehaviour
{
    private static RoadsManager _instance;
    public static RoadsManager Instance => _instance;

    public delegate void RoadCreated(Road road);

    public delegate void RoadDestroyed(Road road);

    public static event RoadCreated OnRoadCreated;
    public static event RoadDestroyed OnRoadDestroyed;

    private NavMeshSurface _roadsParent;
    private RoadMesh _roadMeshPrefab;
    private int _hermiteDivisionsCount;
    private int _hillPeaksCount;
    private List<ARCube> _cubes;
    private List<Road> _roads;

    public static float RoadHeight => GameManager.RoadHeight;
    public static float RoadWidth => GameManager.RoadWidth;
    public static float RoadWallHeight => GameManager.RoadWallHeight;
    public static float RoadWallWidth => GameManager.RoadWallWidth;

    // Returns cubes position translated to road's surface level (adding height/2)
    public List<Vector3> GetControlPoints()
    {
        return _cubes
            .Select(cube => cube.transform.position + cube.transform.up * RoadHeight / 2)
            .ToList();
    }

    private void Awake()
    {
        Debug.Log("Roads manager awake");
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(this);
            return;
        }

        _instance = this;

        _cubes = new List<ARCube>();
        _roads = new List<Road>();

        CubeManager.OnCubeCreated += OnCubeCreated;
        CubeManager.OnCubeDestroyed += OnCubeDestroyed;
    }

    private void Start()
    {
        _roadsParent = GameManager.RoadsParent;
        _roadMeshPrefab = GameManager.RoadMeshPrefab;
        _hermiteDivisionsCount = GameManager.HermiteDivisionsCount;
        _hillPeaksCount = GameManager.HillPeaksCount;
    }

    private void OnCubeCreated(ARCube cube)
    {
        _cubes.Add(cube);
        Debug.Log($"RoadsManager +cube count: {_cubes.Count}");
        if (_cubes.Count == 1)
        {
            return;
        }

        // CreateRoadPath(_cubes[_cubes.Count - 2], cube);
        // ReconstructRoadPath();
        RefreshRoadMeshes();
    }

    private void RefreshRoadMeshes()
    {
        Debug.Log("Refreshing all road meshes");
        if (_cubes.Count < 2)
        {
            return;
        }

        _roads.ForEach(road =>
        {
            // anchorManager.RemoveAnchor(road.ARAnchor);
            road.Destroy();
        });
        _roads.Clear();

        // List<Vector3> controlPoints = _cubes.Select(cube => cube.transform.position + Vector3.up).ToList();
        for (int i = 0; i < _cubes.Count; i++)
        {
            int prev = i - 1 < 0 ? _cubes.Count - 1 : i - 1;
            int p0 = i;
            int p1 = (i + 1) % _cubes.Count;
            int next = (i + 2) % _cubes.Count;
            Debug.Log($"Indices: p-1: {prev}, p0: {p0}, p1: {p1}, p2: {next}");
            List<Vector3> points = Utils.GetHermitePoints(
                _cubes[prev].transform.position,
                _cubes[p0].transform.position,
                _cubes[p1].transform.position,
                _cubes[next].transform.position,
                _hermiteDivisionsCount);


            List<Vector3> randomWalkSegment = Utils.RandomWalkHermiteNew(points, _hillPeaksCount, 1f);
            RoadMesh roadMesh = Instantiate(_roadMeshPrefab, points[0], Quaternion.identity);
            roadMesh.PathPoints = randomWalkSegment.ToArray();
            roadMesh.transform.SetParent(_roadsParent.transform, true);
            // ARAnchor anchor = anchorManager.AddAnchor(new Pose(points[0], Quaternion.identity));
            // roadMesh.transform.SetParent(anchor.transform, true);
            // roadMesh.gameObject.AddComponent<ARAnchor>(); // TODO: Test if adding it directly to prefab is enough
            Road road = new Road(_cubes[p0], _cubes[p1], roadMesh.gameObject, null);
            _roads.Add(road);
        }

        for (int i = 0; i < _roads.Count; i++)
        {
            int prev = i - 1 < 0 ? _roads.Count - 1 : i - 1;
            RoadMesh prevRoad = _roads[prev].RoadGameObject.GetComponent<RoadMesh>();
            RoadMesh currentRoad = _roads[i].RoadGameObject.GetComponent<RoadMesh>();
            prevRoad.ConnectToNext(currentRoad);
        }
    }

    private void OnCubeDestroyed(ARCube cube)
    {
        List<Road> toRemove = new List<Road>();
        foreach (Road road in _roads)
        {
            if (ReferenceEquals(road.From, cube) || ReferenceEquals(road.To, cube))
            {
                Debug.Log($"Trying to destroy {road.RoadGameObject.GetHashCode()}");
                Destroy(road.RoadGameObject);
                OnRoadDestroyed?.Invoke(road);
            }

            toRemove.Add(road);
        }

        _roads.RemoveAll(road => toRemove.Contains(road));
        _cubes.Remove(cube);
        Debug.Log($"RoadsManager -cube count: {_cubes.Count}");
    }
}