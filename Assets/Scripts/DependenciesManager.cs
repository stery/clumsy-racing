﻿// ReSharper disable once RedundantUsingDirective
using System;
using AR;
using Cars.Drivers;
using InputProviders;
using Segments;
using Segments.ControlPoints;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.XR.ARFoundation;
using Object = UnityEngine.Object;

public class DependenciesManager : MonoBehaviour
{
    // !! Make sure this is the first script in the execution order
    private static DependenciesManager _instance;

    private GameConfiguration _gameConfiguration;
    private DefaultPlayerInput _defaultPlayerInput;
    private IInputProvider _inputProvider;
    private IARManager _arManager;
    private ControlPointStore _controlPointStore;
    private SegmentStore _segmentStore;
    private AbstractControlPointGenerator _controlPointGenerator;
    private SegmentGenerator _segmentGenerator;
    // private PlayerCarDriver _playerCarDriver;
    // private BotCarDriver[] _botCarDrivers;
    private SceneParent _sceneParent;
    private NavMeshSurface _navMeshSurface;
    private INavigationDataProvider _navigationDataProvider;
    private PathPreview _lastSegmentPreview;

    public static GameConfiguration GameConfiguration => _instance._gameConfiguration;
    public static IInputProvider InputProvider => _instance._inputProvider;
    public static IARManager ARManager => _instance._arManager;
    public static ControlPointStore ControlPointStore => _instance._controlPointStore;
    public static SegmentStore SegmentStore => _instance._segmentStore;
    public static SegmentGenerator SegmentGenerator => _instance._segmentGenerator; // TODO(optimize-edit-mode): temporary
    // public static PlayerCarDriver PlayerCarDriver => _instance._playerCarDriver;
    // public static BotCarDriver[] BotCarDrivers => _instance._botCarDrivers;
    public static SceneParent SceneParent => _instance._sceneParent;
    public static INavigationDataProvider NavigationDataProvider => _instance._navigationDataProvider;
    public static PathPreview LastSegmentPreview => _instance._lastSegmentPreview;

    private void Awake()
    {
        Debug.Log($"Dependencies Manager awake, instance is {_instance}");

        DependenciesManager[] dependenciesManagers = FindObjectsOfType<DependenciesManager>();
        if (dependenciesManagers.Length > 1)
        {
            Debug.LogWarning("Multiple dependencies managers detected");
            if (this != dependenciesManagers[0])
            {
                return;
            }

            for (int i = 1; i < dependenciesManagers.Length; i++)
            {
                DestroyImmediate(dependenciesManagers[i]);
            }
        }

        _instance = dependenciesManagers[0];

        // if (_instance != null && _instance != this)
        // {
        //     DestroyImmediate(this);
        //     return;
        // }
        //
        // _instance = this;

        InitDependencies();
    }

    private void Start()
    {
        if (_instance == null)
        {
            throw new InvalidProgramException("Instance is still null");
        }
    }

    private void OnDestroy() // Scene reload, maybe
    {
        Debug.Log("Dependencies Manager destroyed, setting instance and deps to null");
        _instance = null;

    _gameConfiguration = null;
    _defaultPlayerInput = null;
    _inputProvider.Destroy();
    _inputProvider = null;
    _arManager = null;
    _controlPointStore = null;
    _segmentStore = null;
    _controlPointGenerator.Destroy();
    _controlPointGenerator = null;
    _segmentGenerator.Destroy();
    _segmentGenerator = null;
    _sceneParent = null;
    _navMeshSurface = null;
    _navigationDataProvider = null;
    // Destroy(_lastSegmentPreview.gameObject); // The GameObject already gets marked for destroy
    _lastSegmentPreview = null;

    GC.Collect();
    }

    private void InitDependencies()
    {
        _gameConfiguration = TryFindObjectOfType<GameConfiguration>();
        _defaultPlayerInput = new DefaultPlayerInput();
        _inputProvider = new ActionsInputProvider(_defaultPlayerInput);
        _controlPointStore = new ControlPointStore();
        _segmentStore = TryFindObjectOfType<SegmentStore>();

#if UNITY_EDITOR
        Debug.Log("Unity editor");
        // _inputProvider = new DebugInputProvider();
        _arManager = new MockARManager();
        _controlPointGenerator = new DebugControlPointGenerator(_inputProvider,
            _controlPointStore,
            _gameConfiguration.ControlPointPrefab);
        _sceneParent = new GameObject("SceneParent").AddComponent<SceneParent>();
#else
        Debug.Log("Not unity editor");
        // _inputProvider = new GUIInputProvider();
        _arManager = new ARManager(TryFindObjectOfType<AROcclusionManager>(),
                TryFindObjectOfType<ARPlaneManager>(),
                TryFindObjectOfType<ARRaycastManager>());
        _controlPointGenerator = new ARControlPointGenerator(_inputProvider,
            _controlPointStore,
            _gameConfiguration.ControlPointPrefab,
            _arManager);
        _sceneParent = new GameObject("ARSceneParent").AddComponent<ARSceneParent>();
#endif

        _segmentGenerator = new SegmentGenerator(_segmentStore, _controlPointStore,
            _gameConfiguration.HermiteDivisionsCount, _gameConfiguration.HillPeaksCount);
        // _playerCarDriver = TryFindObjectOfType<PlayerCarDriver>();
        // _botCarDrivers = FindObjectsOfType<BotCarDriver>();
        _navMeshSurface = TryFindObjectOfType<NavMeshSurface>();
        _navigationDataProvider = new NavMeshDataProvider(_controlPointStore, _navMeshSurface);
        _lastSegmentPreview = new GameObject("LastSegmentPreview").AddComponent<PathPreview>();
    }

    private T TryFindObjectOfType<T>() where T : Object
    {
        T t = FindObjectOfType<T>();

        if (t == null)
        {
            throw new InvalidOperationException($"{typeof(T).Name} is not set");
        }

        return t;
    }
}