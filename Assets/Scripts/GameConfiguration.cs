﻿using System.Collections.Generic;
using System.Linq;
using Cars.Drivers;
using Meshes;
using Meshes.RoadSide;
using Segments.ControlPoints;
using UnityEngine;

public class GameConfiguration : MonoBehaviour
{
    [Header("Road settings")] //
    [SerializeField]
    private float roadHeight;
    [SerializeField] private float roadWidth;
    [SerializeField] private float roadWallHeight;
    [SerializeField] private float roadWallWidth;
    [SerializeField] private int hermiteDivisionsCount = 100;
    [SerializeField] private int hillPeaksCount = 10;
    [Header("Road side settings")]
    [SerializeField] private float roadSideWidth;
    [SerializeField] private int roadSideExtrasRetryCount;

    [Header("Cars")] //
    [SerializeField]
    private float maxSpeed;
    [SerializeField] private float acceleration;
    [SerializeField] private float maxSteeringAngle;

    [Header("Prefabs")] //
    [SerializeField]
    private ControlPoint controlPointPrefab;
    [SerializeField] private RoadMesh roadMeshPrefab;
    [SerializeField] private PlayerCarDriver playerCarDriverPrefab;
    [SerializeField] private BotCarDriver botCarDriverPrefab;
    [SerializeField] private RoadSideSO[] roadSides;

    [Header("Debug printing")] //
    [SerializeField]
    private bool enableDebugLogs = true;

    public float RoadHeight => roadHeight;
    public float RoadWidth => roadWidth;
    public float RoadWallHeight => roadWallHeight;
    public float RoadWallWidth => roadWallWidth;
    public int HermiteDivisionsCount => hermiteDivisionsCount;
    public int HillPeaksCount => hillPeaksCount;
    public float RoadSideWidth => roadSideWidth;
    public int RoadSideExtrasRetryCount => roadSideExtrasRetryCount;
    public float MAXSpeed => maxSpeed;
    public float Acceleration => acceleration;
    public float MAXSteeringAngle => maxSteeringAngle;
    public ControlPoint ControlPointPrefab => controlPointPrefab;
    public RoadMesh RoadMeshPrefab => roadMeshPrefab;
    public PlayerCarDriver PlayerCarDriverPrefab => playerCarDriverPrefab;
    public BotCarDriver BotCarDriverPrefab => botCarDriverPrefab;
    public List<RoadSideData> RoadSideData => roadSides.Select(roadSide => roadSide.data).ToList();
    public bool EnableDebugLogs => enableDebugLogs;
}