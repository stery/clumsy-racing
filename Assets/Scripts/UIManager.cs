﻿using System;
using Cars.Drivers;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Button addButton;
    [SerializeField] private Button removeButton;
    [SerializeField] private Button doneButton;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button unstuckButton;
    
    // Debugging
    [Header("Debugging")]
    [SerializeField] private Text modeText;
    [SerializeField] private Text playerVelocityText;
    [SerializeField] private Text botVelocityText;

    private Rigidbody _playerRB;
    private Rigidbody _botRB;

    private void OnEnable()
    {
        GameManager.OnGameStateChanged += OnGameStateChanged;
    }

    private void OnDisable()
    {
        GameManager.OnGameStateChanged -= OnGameStateChanged;
    }

    private void FixedUpdate()
    {
        GameManager.GameState currentState = GameManager.CurrentGameState;
        modeText.text = $"Mode: {currentState}";
        if (currentState != GameManager.GameState.Playing)
        {
            return;
        }

        _playerRB ??= FindObjectOfType<PlayerCarDriver>().GetComponent<Rigidbody>();
        _botRB ??= FindObjectOfType<BotCarDriver>().GetComponent<Rigidbody>();

        playerVelocityText.text = $"PVelocity: {_playerRB.velocity}";
        botVelocityText.text = $"BVelocity: {_botRB.velocity}";
    }

    // public void OnAddClick()
    // {
    //     Utils.CustomDebugLog(Utils.LogLevel.Info, "Add clicked");
    //     ObjectClickManager.Instance.ClickFilter = ObjectClickManager.ObjectClickFilter.AR;
    //     modeText.text = "Mode: adding";
    // }
    //
    // public void OnRemoveClick()
    // {
    //     Utils.CustomDebugLog(Utils.LogLevel.Info, "Remove clicked");
    //     ObjectClickManager.Instance.ClickFilter = ObjectClickManager.ObjectClickFilter.NonAR;
    //     modeText.text = "Mode: removing";
    // }
    //
    // public void OnDoneClick()
    // {
    //     Utils.CustomDebugLog(Utils.LogLevel.Info, "Done clicked");
    //     GameManager.Instance.ChangeGameState(GameManager.GameState.Playing);
    //     ObjectClickManager.Instance.ClickFilter = ObjectClickManager.ObjectClickFilter.NonAR;
    //     modeText.text = "Mode: Playing";
    // }
    //
    // public void OnRestartClick()
    // {
    //     GameManager.Instance.Restart();
    // }
    //
    // public void OnUnstuckClick()
    // {
    //     GameManager.Instance.Unstuck();
    // }

    private void OnGameStateChanged(GameManager.GameState gameState)
    {
        switch (gameState)
        {
            case GameManager.GameState.EditingRoad:
                EnableEditButtons();
                break;
            case GameManager.GameState.Playing:
                DisableEditButtons();
                break;
            default:
                throw new InvalidOperationException("Unknown game state");
        }
    }
    
    public void DisableEditButtons()
    {
        addButton.interactable = false;
        removeButton.interactable = false;
        doneButton.interactable = false;
    }

    public void EnableEditButtons()
    {
        addButton.interactable = true;
        removeButton.interactable = true;
        doneButton.interactable = true;
    }
}