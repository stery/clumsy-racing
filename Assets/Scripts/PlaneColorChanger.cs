﻿using System;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using Random = UnityEngine.Random;

public class PlaneColorChanger : MonoBehaviour
{
    [SerializeField] private ARPlaneManager arPlaneManager;

    private void Awake()
    {
        if (arPlaneManager == null)
        {
            Utils.CustomDebugLog(Utils.LogLevel.Error, "Ar Plane Manager is not set");
            throw new InvalidOperationException("Ar Plane Manager is not set");
        }
    }

    private void Start()
    {
        arPlaneManager.planesChanged += ArPlanesChangedListener;
    }

    private void OnDestroy()
    {
        arPlaneManager.planesChanged -= ArPlanesChangedListener;
    }

    private void ArPlanesChangedListener(ARPlanesChangedEventArgs arPlanesChangedEventArgs)
    {
        arPlanesChangedEventArgs.added
            .ForEach(plane =>
                plane.GetComponent<MeshRenderer>().material.color =
                    new Color(Random.value, Random.value, Random.value, 0.2f));
    }
}