﻿using AR;
using Cars.Drivers;
using Segments;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(GameConfiguration))]
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public delegate void GameStateEvent(GameState gameState);

    public static event GameStateEvent OnGameStateChanged;

    private IARManager _arManager;

    private GameState gameState;
    // private RoadsManager _roadsManager;
    private PlayerCarDriver _playerCarDriver;
    private BotCarDriver _botCarDriver;
    // private List<Vector3> _navWaypoints;

    // public static NavMeshSurface RoadsParent => Instance.roadsParent;
    public static GameState CurrentGameState => Instance.gameState;
    // public static List<Vector3> NavWaypoints => Instance._navWaypoints;

    private bool _occlusion;
    public bool Occlusion
    {
        get => _occlusion;
        private set
        {
            _occlusion = value;
            _arManager.SetOcclusionActive(_occlusion);
        }
    }

    protected void Awake()
    {
        Debug.Log("Game manager Awake");
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(this);
            return;
        }

        Instance = this;

        _arManager = DependenciesManager.ARManager;
    }

    private void OnDestroy()
    {
        Debug.Log("Game manager destroyed, setting instance to null");
        
        Instance = null;
        OnGameStateChanged = null;
    }

    protected void Start()
    {
        // _arManager.SetOcclusionActive(false);

        // _roadsManager = RoadsManager.Instance;
        // if (roadsParent == null)
        // {
        //     roadsParent = _roadsManager.transform.GetComponent<NavMeshSurface>();
        // }

        ChangeGameState(GameState.EditingRoad);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ToggleOcclusion()
    {
        Occlusion = !Occlusion;
    }

    public void ChangeGameState(GameState targetState)
    {
        if (targetState == gameState)
        {
            Utils.CustomDebugLog(Utils.LogLevel.Warn, $"Target state is already active ({targetState})");
            return;
        }

        // Emit event
        
        gameState = targetState;
        OnGameStateChanged?.Invoke(targetState);

        PathPreview lastSegmentPreview = DependenciesManager.LastSegmentPreview;

        switch (targetState)
        {
            case GameState.Playing:
                Occlusion = true;
                _arManager.SetPlaneDetectionActive(false);
                lastSegmentPreview.gameObject.SetActive(false);

                Play();
                break;
            case GameState.EditingRoad:
                Occlusion = false;
                _arManager.SetPlaneDetectionActive(true);
                lastSegmentPreview.gameObject.SetActive(true);
                lastSegmentPreview.SetPathPoints(new Vector3[0]);
                // UIManager.Instance.EnableEditButtons();
                break;
            default:
                break;
        }
    }

    private void Play()
    {
        // UIManager.Instance.DisableEditButtons();
        // // Bake navmesh
        // NavMesh.RemoveAllNavMeshData();
        // RoadsParent.BuildNavMesh();
        // FindObjectOfType<DebugNavMeshRenderer>()?.UpdateMeshRender();
        //
        // // Bake navmesh path for bots
        // _navWaypoints = new List<Vector3>();
        // List<Vector3> controlPoints = _roadsManager.GetControlPoints();
        // int controlPointsCount = controlPoints.Count;
        // NavMeshPath navMeshPath = new NavMeshPath();
        // for (int i = 0; i < controlPointsCount; i++)
        // {
        //     int iNext = (i + 1) % controlPointsCount;
        //     Vector3 currentCp = controlPoints[i];
        //     Vector3 nextCp = controlPoints[iNext];
        //     NavMesh.CalculatePath(currentCp, nextCp, 1 << 0, navMeshPath);
        //     _navWaypoints.AddRange(navMeshPath.corners);
        // }
        //
        // _navWaypoints = _navWaypoints.Distinct().ToList();

        // Spawn cars
        GameConfiguration gameConfiguration = DependenciesManager.GameConfiguration;

        Transform firstRoadTransform = DependenciesManager.SegmentStore.Segments[0].transform;
        Utils.CustomDebugLog(Utils.LogLevel.Info, $"First road rotation: {firstRoadTransform.rotation}");
        _playerCarDriver = Instantiate(gameConfiguration.PlayerCarDriverPrefab,
            firstRoadTransform.position - firstRoadTransform.right * (gameConfiguration.RoadWidth / 4)
            + firstRoadTransform.up * 0.5f,
            firstRoadTransform.rotation);
        _botCarDriver = Instantiate(gameConfiguration.BotCarDriverPrefab,
            firstRoadTransform.position + firstRoadTransform.right * (gameConfiguration.RoadWidth / 4)
                                        + firstRoadTransform.up * 0.5f,
            firstRoadTransform.rotation);

        // TODO: Create final anchor on roads & car parent, remove anchors from road meshes
        Transform sceneParentTransform = DependenciesManager.SceneParent.transform;
        // Transform anchoredRoot = new GameObject("Anchored root").transform;
        // foreach (Transform road in roadsParent.transform)
        // {
        //     Destroy(road.GetComponent<ARAnchor>());
        // }

        // roadsParent.transform.SetParent(sceneParentTransform, true); // TODO: Test if positions stay
        Transform carsParent = new GameObject("Cars parent").transform;
        carsParent.SetParent(sceneParentTransform);
        _playerCarDriver.transform.SetParent(carsParent, true);
        _botCarDriver.transform.SetParent(carsParent, true);
        // anchoredRoot.gameObject.AddComponent<ARAnchor>();
        _playerCarDriver.transform.rotation = firstRoadTransform.rotation;
        _playerCarDriver.transform.localPosition += Vector3.up * 2f;
        _botCarDriver.transform.rotation = firstRoadTransform.rotation;
        _botCarDriver.transform.localPosition += Vector3.up * 2f;
    }

    public void Restart()
    {
        Utils.CustomDebugLog(Utils.LogLevel.Warn, "Restarting game");
        SceneManager.LoadScene(0);
    }

    public void Unstuck()
    {
        _playerCarDriver.gameObject.SetActive(false);
        _botCarDriver.gameObject.SetActive(false);
        _playerCarDriver.transform.localPosition += Vector3.up * 2f;
        _botCarDriver.transform.localPosition += Vector3.up * 2f;
        _playerCarDriver.gameObject.SetActive(true);
        _botCarDriver.gameObject.SetActive(true);
    }

    public enum GameState
    {
        EditingRoad,
        Playing
    }
}