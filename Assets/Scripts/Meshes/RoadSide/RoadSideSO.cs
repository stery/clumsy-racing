﻿using UnityEngine;

namespace Meshes.RoadSide
{
    [CreateAssetMenu(fileName = "roadSideData", menuName = "items/roadSideData", order = 0)]
    public class RoadSideSO : ScriptableObject
    {
        public RoadSideData data;
    }
}