﻿using System;
using UnityEngine;

namespace Meshes.RoadSide
{
    [Serializable]
    public struct RoadSideData
    {
        public RoadSideType type;
        public Material material;
        // Extras
        public int density;
        public GameObject[] extraPrefabs;
    }
}