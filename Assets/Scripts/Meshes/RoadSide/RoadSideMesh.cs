﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Meshes.RoadSide
{
    public class RoadSideMesh : ProceduralMesh
    {
        private RoadSideData _roadSideData;
        private int _retryCount;
        // private MeshRenderer _renderer;

        // Bounds
        private Bounds _roadBounds;
        private List<Bounds> _extrasBounds;

        // Extras references
        private List<Transform> _extrasTransforms;
        private bool _reusable;

        public void Init(float width, float height,
                         Vector3[] pathPoints, RoadSideData roadSideData,
                         int retryCount, Bounds roadBounds)
        {
            Mesh = new Mesh();
            _roadSideData = roadSideData;
            _retryCount = retryCount;

            _roadBounds = roadBounds;
            _extrasBounds = new List<Bounds>();

            _extrasTransforms = new List<Transform>();

            MeshFilter = GetComponent<MeshFilter>();
            MeshFilter = MeshFilter ? MeshFilter : gameObject.AddComponent<MeshFilter>();
            MeshFilter.mesh = Mesh;

            // _renderer = GetComponent<MeshRenderer>();
            // _renderer = _renderer ? _renderer : gameObject.AddComponent<MeshRenderer>();

            MeshRenderer.material = _roadSideData.material;

            base.Init(width, height, pathPoints);

            // SpawnExtras();
        }

        public void UpdateRoadBounds(Bounds roadBounds)
        {
            _roadBounds = roadBounds;
        }

        public void SpawnExtras()
        {
            if (_reusable)
            {
                _extrasBounds.Clear();
                _extrasTransforms
                    .Select(extra => extra.gameObject)
                    .ToList()
                    .ForEach(extra => SpawnExtra(extra, false));
                
                return;
            }
            
            GameObject[] extraPrefabs = _roadSideData.extraPrefabs;
            
            if (extraPrefabs.Length == 0) return;
            
            int density = _roadSideData.density;
            for (int i = 0; i < density; ++i)
            {
                GameObject extraPrefab = extraPrefabs[Random.Range(0, extraPrefabs.Length)];
                SpawnExtra(extraPrefab, true);
            }

            _reusable = true;
        }

        public void SpawnExtrasOld()
        {
            GameObject[] extraPrefabs = _roadSideData.extraPrefabs;

            if (extraPrefabs.Length == 0) return;

            int density = _roadSideData.density;

            for (int i = 0; i < density; ++i)
            {
                GameObject extraPrefab = extraPrefabs[Random.Range(0, extraPrefabs.Length)];
                Vector3 position = Vector3.positiveInfinity;
                bool failed = true;

                for (int attempts = 0; attempts < _retryCount; ++attempts)
                {
                    position = GetRandomPointOnAboveSurface();
                    Bounds bounds = Utils.GetComposedBounds(extraPrefab.transform);

                    bounds.center += position;
                    // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Bounds is {bounds}");

                    // Check if position is not within the road area
                    if (_roadBounds.Intersects(bounds))
                    {
                        // Utils.CustomDebugLog(Utils.LogLevel.Info, "Road overlap, skipping");
                        continue;
                    }

                    // Check if position is not within other's bounding box
                    if (_extrasBounds.Any(b => b.Intersects(bounds)))
                    {
                        // Utils.CustomDebugLog(Utils.LogLevel.Info, "Overlap detected, skipping");
                        continue;
                    }

                    failed = false;
                    break;
                }

                if (failed)
                {
                    // Utils.CustomDebugLog(Utils.LogLevel.Warn, $"Failed to find a suitable position for extra {extraPrefab.name}");
                    continue;
                }

                Transform extra = Instantiate(extraPrefab, transform, false).transform;
                // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Setting position to {position}");
                Vector3 closestPoint =
                    Utils.GetClosestPoint(PathPoints.Select(point => point + transform.position).ToList(), position);
                Quaternion rotation = Quaternion.LookRotation((closestPoint - position).normalized, Vector3.up);
                // Utils.CustomDebugLog(Utils.LogLevel.Info, 
                //     $"Extra position: {position}, closest path point: {closestPoint}, rotation: {rotation.eulerAngles}");
                extra.SetPositionAndRotation(position, rotation);
                _extrasBounds.Add(Utils.GetComposedBounds(extra));
            }
        }

        private void SpawnExtra(GameObject extraGO, bool instantiate)
        {
            Vector3 position = Vector3.positiveInfinity;
            bool failed = true;

            for (int attempts = 0; attempts < _retryCount; ++attempts)
            {
                position = GetRandomPointOnAboveSurface();
                Bounds bounds = Utils.GetComposedBounds(extraGO.transform);

                bounds.center = position;
                // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Bounds is {bounds}");


                // Check if position is not within the road area
                if (_roadBounds.Intersects(bounds))
                {
                    // Utils.CustomDebugLog(Utils.LogLevel.Info, "Road overlap, skipping");
                    continue;
                }

                // Check if position is not within other's bounding box
                if (_extrasBounds.Any(b => b.Intersects(bounds)))
                {
                    // Utils.CustomDebugLog(Utils.LogLevel.Info, "Overlap detected, skipping");
                    continue;
                }

                failed = false;
                break;
            }

            if (failed)
            {
                // Utils.CustomDebugLog(Utils.LogLevel.Warn, $"Failed to find a suitable position for extra {extraGO.name}");
                return;
            }

            Transform extra = instantiate
                ? Instantiate(extraGO, transform, false).transform
                : extraGO.transform;
            // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Setting position to {position}");
            Vector3 closestPoint = Utils.GetClosestPoint(
                PathPoints.Select(point => point + transform.position).ToList(),
                position);
            Quaternion rotation = Quaternion.LookRotation((closestPoint - position).normalized, Vector3.up);
            // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Extra position: {position}, " +
            //           $"closest path point: {closestPoint}, " +
            //           $"rotation: {rotation.eulerAngles}");

            extra.SetPositionAndRotation(position, rotation);
            _extrasBounds.Add(Utils.GetComposedBounds(extra));
            
            if (instantiate)
            {
                _extrasTransforms.Add(extra);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireCube(Bounds.center, Bounds.size);

            Gizmos.color = Color.green;
            _extrasBounds.ForEach(b => Gizmos.DrawWireCube(b.center, b.size));
        }
    }
}