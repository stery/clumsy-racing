﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Meshes
{
    [RequireComponent(typeof(MeshFilter))]
    public class ProceduralMesh : MonoBehaviour
    {
        protected MeshFilter MeshFilter;
        protected MeshCollider MeshCollider;
        protected MeshRenderer MeshRenderer; // For bounds
        protected Mesh Mesh;

        protected float _width;
        protected float _height;
        protected Vector3[] _pathPoints;
        protected bool _withCaps;

        private int[] _aboveTriangleIds; // For computing random points on above surface
        private int[] _aboveVertexIds; // For textures
        private Material _material;

        private float _pathLength;
        private float[] _distanceBetweenPathPoints;

        public Vector3[] PathPoints
        {
            get => _pathPoints;
            set
            {
                _pathPoints = value;
                Utils.CustomDebugLog(Utils.LogLevel.Info, $"{name} has paths set to {value} ({value.Length} size)");
                PathPointsToLocalCoords();

                // Cache length if has texture
                if (_material != null && _material.mainTexture != null)
                {
                    _distanceBetweenPathPoints = Utils.ComputeDistanceBetweenPathPoints(_pathPoints);
                    _pathLength = _distanceBetweenPathPoints.Sum();
                }

                UpdateMesh();
            }
        }

        public Bounds Bounds => MeshRenderer.bounds;

        protected virtual void Awake()
        {
            MeshFilter = GetComponent<MeshFilter>();
            MeshCollider = GetComponent<MeshCollider>();
            MeshRenderer = GetComponent<MeshRenderer>();
            if (MeshRenderer != null)
            {
                _material = MeshRenderer.material;
            }
            else
            {
                MeshRenderer = gameObject.AddComponent<MeshRenderer>();
            }

            Mesh = Mesh ? Mesh : MeshFilter.mesh;
        }

        public void Init(float width, float height, Vector3[] pathPoints)
        {
            _width = width;
            _height = height;
            PathPoints = pathPoints;
        }

        protected void PathPointsToLocalCoords()
        {
            // Make the path start from local origin
            Vector3 offset = _pathPoints[0];
            for (int i = 0; i < _pathPoints.Length; i++)
            {
                _pathPoints[i] -= offset;
            }
        }

        protected virtual void UpdateMesh()
        {
            UpdateMesh(true);
        }

        protected void UpdateMesh(bool withCaps)
        {
            int pointsCount = _pathPoints.Length;
            if (pointsCount < 2)
            {
                Utils.CustomDebugLog(Utils.LogLevel.Warn, "Can't update mesh with less than 2 points");
                return;
            }

            Utils.CustomDebugLog(Utils.LogLevel.Info, $"Road height: {_height}, width: {_width}");

            Mesh.Clear();

            Vector3[] vertices = new Vector3[pointsCount * 4];
            Vector3[] normals = new Vector3[pointsCount * 4];
            /*
             * pointsCount - 1 because it needs two points to make a parallelepiped
             * 8 * 3 because there are 4 faces -> 8 triangles (excluding "caps", just a "tube")
             * 4 * 3 because there are 2 faces -> 4 triangles for the "caps"
             */
            // int trianglesCount = (pointsCount - 1) * 8 * 3 + (withCaps ? 4 * 3 : 0);
            int trianglesCount = (pointsCount - 1) * 8 * 3 + 4 * 3;
            int[] triangles = new int[trianglesCount];
            _aboveTriangleIds = new int[(pointsCount - 1) * 2 * 3];
            _aboveVertexIds = new int[pointsCount * 2]; // For textures

            Vector3 left = Vector3.zero;

            for (int i = 0; i < pointsCount - 1; i++)
            {
                Vector3 currentPoint = _pathPoints[i];
                Vector3 nextPoint = _pathPoints[i + 1];
                Vector3 forward = (nextPoint - currentPoint).normalized;
                left = Vector3.Cross(Vector3.up, forward).normalized;

                // Vertices
                Vector3 leftBelowVertex = currentPoint + left * (_width / 2);
                Vector3 rightBelowVertex = currentPoint - left * (_width / 2);
                Vector3 leftAboveVertex = leftBelowVertex + Vector3.up * _height;
                Vector3 rightAboveVertex = rightBelowVertex + Vector3.up * _height;
                vertices[4 * i] = leftAboveVertex;
                vertices[4 * i + 1] = rightAboveVertex;
                vertices[4 * i + 2] = rightBelowVertex;
                vertices[4 * i + 3] = leftBelowVertex;

                _aboveVertexIds[2 * i] = 4 * i;
                _aboveVertexIds[2 * i + 1] = 4 * i + 1;

                // Triangles
                // above lower-left-triangle
                triangles[24 * i] = 4 * i;
                triangles[24 * i + 1] = 4 * i + 1;
                triangles[24 * i + 2] = 4 * (i + 1);
                // above upper-right-triangle
                triangles[24 * i + 3] = 4 * i + 1;
                triangles[24 * i + 4] = 4 * (i + 1) + 1;
                triangles[24 * i + 5] = 4 * (i + 1);
                // cache above triangles for random point on above surface
                for (int j = 0; j < 6; ++j)
                {
                    _aboveTriangleIds[6 * i + j] = 24 * i + j;
                }

                // below lower-left-triangle
                triangles[24 * i + 6] = 4 * i + 2;
                triangles[24 * i + 7] = 4 * i + 3;
                triangles[24 * i + 8] = 4 * (i + 1) + 3;
                // below upper-right-triangle
                triangles[24 * i + 9] = 4 * i + 2;
                triangles[24 * i + 10] = 4 * (i + 1) + 3;
                triangles[24 * i + 11] = 4 * (i + 1) + 2;
                // left side lower-left-triangle
                triangles[24 * i + 12] = 4 * i + 3;
                triangles[24 * i + 13] = 4 * (i + 1);
                triangles[24 * i + 14] = 4 * (i + 1) + 3;
                // left side upper-right-triangle
                triangles[24 * i + 15] = 4 * i;
                triangles[24 * i + 16] = 4 * (i + 1);
                triangles[24 * i + 17] = 4 * i + 3;
                // right side lower-left-triangle
                triangles[24 * i + 18] = 4 * i + 1;
                triangles[24 * i + 19] = 4 * i + 2;
                triangles[24 * i + 20] = 4 * (i + 1) + 2;
                // right side upper-right-triangle
                triangles[24 * i + 21] = 4 * i + 1;
                triangles[24 * i + 22] = 4 * (i + 1) + 2;
                triangles[24 * i + 23] = 4 * (i + 1) + 1;
            }

            // Last vertices
            Vector3 leftBelow = _pathPoints[pointsCount - 1] + left * (_width / 2);
            Vector3 rightBelow = _pathPoints[pointsCount - 1] - left * (_width / 2);
            vertices[pointsCount * 4 - 4] = leftBelow + Vector3.up * _height;
            vertices[pointsCount * 4 - 3] = rightBelow + Vector3.up * _height;
            vertices[pointsCount * 4 - 2] = rightBelow;
            vertices[pointsCount * 4 - 1] = leftBelow;

            _aboveVertexIds[pointsCount * 2 - 2] = pointsCount * 4 - 4;
            _aboveVertexIds[pointsCount * 2 - 1] = pointsCount * 4 - 3;

            // caps triangles (start/end)
            if (withCaps)
            {
                triangles[24 * (pointsCount - 1)] = 0;
                triangles[24 * (pointsCount - 1) + 1] = 3;
                triangles[24 * (pointsCount - 1) + 2] = 2;
                triangles[24 * (pointsCount - 1) + 3] = 0;
                triangles[24 * (pointsCount - 1) + 4] = 2;
                triangles[24 * (pointsCount - 1) + 5] = 1;

                triangles[24 * (pointsCount - 1) + 6] = pointsCount * 4 - 1;
                triangles[24 * (pointsCount - 1) + 7] = pointsCount * 4 - 4;
                triangles[24 * (pointsCount - 1) + 8] = pointsCount * 4 - 3;
                triangles[24 * (pointsCount - 1) + 9] = pointsCount * 4 - 3;
                triangles[24 * (pointsCount - 1) + 10] = pointsCount * 4 - 2;
                triangles[24 * (pointsCount - 1) + 11] = pointsCount * 4 - 1;
            }

            Mesh.vertices = vertices;
            Mesh.triangles = triangles;
            _withCaps = withCaps;
            RecalculateUVs();
            Mesh.RecalculateBounds();
            Mesh.RecalculateNormals();

            if (MeshCollider != null)
            {
                MeshCollider.sharedMesh = Mesh;
            }
        }

        protected virtual void RecalculateUVs()
        {
            int pointsCount = _pathPoints.Length;
            if (pointsCount < 2)
            {
                Utils.CustomDebugLog(Utils.LogLevel.Warn, "Can't update UVs for mesh with less than 2 points");
                return;
            }

            Vector2[] uvs = new Vector2[pointsCount * 4];
            Texture texture = _material != null ? _material.mainTexture : null;

            if (texture == null)
            {
                return;
            }

            int repeatCount = Math.Max((int) (_pathLength / texture.height), 1) * 10;
            _material.mainTextureScale = new Vector2(1f, repeatCount);

            int distancesCount = _distanceBetweenPathPoints.Length;
            float[] normalizedDistances = new float[distancesCount];
            float sum = 0f;
            for (int i = 0; i < distancesCount; ++i)
            {
                sum += _distanceBetweenPathPoints[i];
                normalizedDistances[i] = sum / _pathLength;
            }
                
            int verticesCount = _aboveVertexIds.Length;
                
            for (int i = 0; i < verticesCount; i += 2)
            {
                int leftId = _aboveVertexIds[i];
                int rightId = _aboveVertexIds[i + 1];
                
                float y = normalizedDistances[i / 2];
                uvs[leftId] = new Vector2(0f, y);
                uvs[rightId] = new Vector2(1f, y);
                    
                // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Setting uv.y = {y} (delta ~{normalizedDistances[1]})");
            }

                

            // int repeatCount = Math.Max((int) (_pathLength / texture.height), 1) * 10;
            // float offset = pointsCount / (float) repeatCount;
            // float totalTextureLength = offset * pointsCount;
            // float normalizedOffset = pointsCount / totalTextureLength;
            // float y = 0f;
            //
            // int verticesCount = _aboveVertexIds.Length;
            // Utils.CustomDebugLog(Utils.LogLevel.Info, "Texture details: " +
            //           $"repeatCount: {repeatCount}, " +
            //           $"offset: {offset}, " +
            //           $"total texture length: {totalTextureLength}, " +
            //           $"normalized offset: {normalizedOffset}, " +
            //           $"texture height: {texture.height}, " +
            //           $"path length: {_pathLength}, " +
            //           $"points count: {pointsCount}");
            //
            // for (int i = 0; i < verticesCount; i += 2)
            // {
            //     int leftId = _aboveVertexIds[i];
            //     int rightId = _aboveVertexIds[i + 1];
            //     
            //     // TODO(texture): Check if it repeats/tiles by going over 1.0 in uv 
            //     Vector2 leftUv = new Vector2(0f, y);
            //     Vector2 rightUv = new Vector2(1f, y);
            //
            //     uvs[leftId] = leftUv;
            //     uvs[rightId] = rightUv;
            //
            //     y += normalizedOffset;
            // }

            Mesh.uv = uvs;
        }

        public virtual void ConnectToNext(ProceduralMesh next)
        {
            Vector3[] vertices = Mesh.vertices;
            int[] triangles = Mesh.triangles;
            int trianglesCount = triangles.Length;

            if (_withCaps)
            {
                int[] nextTriangles = next.Mesh.triangles;
                int nextTrianglesCount = nextTriangles.Length;

                for (int i = 1; i <= 6; i++)
                {
                    // current's end caps
                    triangles[trianglesCount - i] = 0;
                    // next's start caps, assuming connecting to same type of mesh
                    nextTriangles[nextTrianglesCount - 6 - i] = 0;
                }
            }

            int pointsCount = _pathPoints.Length;
            int verticesCount = vertices.Length;
            Vector3 offset = next.transform.position - transform.position; // Vertices are local pos
            Mesh.Clear();

            Utils.CustomDebugLog(Utils.LogLevel.Info, $"Vertices count: {verticesCount}; pointsCount: {pointsCount}\n" +
                      $"next mesh vertices: {next.Mesh.vertices}");

            vertices[verticesCount - 4] = next.Mesh.vertices[0] + offset;
            vertices[verticesCount - 3] = next.Mesh.vertices[1] + offset;
            vertices[verticesCount - 2] = next.Mesh.vertices[2] + offset;
            vertices[verticesCount - 1] = next.Mesh.vertices[3] + offset;

            Mesh.vertices = vertices;
            Mesh.triangles = triangles;
            RecalculateUVs();
            Mesh.RecalculateBounds();
            Mesh.RecalculateNormals();

            if (MeshCollider != null)
            {
                MeshCollider.sharedMesh = Mesh;
            }
        }

        public Vector3 GetRandomPointOnAboveSurface()
        {
            int triangle = Random.Range(0, _aboveTriangleIds.Length - 3);

            Vector3 p1 = Mesh.vertices[Mesh.triangles[_aboveTriangleIds[triangle]]];
            Vector3 p2 = Mesh.vertices[Mesh.triangles[_aboveTriangleIds[triangle + 1]]];
            Vector3 p3 = Mesh.vertices[Mesh.triangles[_aboveTriangleIds[triangle + 2]]];

            float r1 = Mathf.Sqrt(Random.Range(0f, 1f));
            float r2 = Random.Range(0f, 1f);

            float m1 = 1 - r1;
            float m2 = r1 * (1 - r2);
            float m3 = r1 * r2;

            Vector3 position = m1 * p1 + m2 * p2 + m3 * p3;

            return transform.TransformPoint(position);
        }
    }
}