﻿using UnityEngine;

namespace Meshes
{
    public class RoadMesh : ProceduralMesh
    {
        [SerializeField] private ProceduralMesh leftWall;
        [SerializeField] private ProceduralMesh rightWall;
        private float _wallWidth;
        private float _wallHeight;
        // TODO: next point after path ends
        // TODO: Normals

        protected override void Awake()
        {
            // Awake should be fine as long as there are no roadmeshes before the game starts
            // Doing it in Start would cause exceptions because the path is set right after instantiate
            base.Awake();
        }

        public void Init(float width, float height, Vector3[] pathPoints, float wallWidth, float wallHeight)
        {
            _wallWidth = wallWidth;
            _wallHeight = wallHeight;

            base.Init(width, height, pathPoints);
            /*
             * No need to init walls because this.PathPoints assignment
             * will trigger UpdateMesh() which triggers UpdateWalls()
             * which Inits them
             */
            
            Utils.CustomDebugLog(Utils.LogLevel.Info, $"Initialising with Road height: {_height}, width: {_width}");
            Utils.CustomDebugLog(Utils.LogLevel.Info, "RoadMesh initialised");
        }

        protected override void UpdateMesh()
        {
            UpdateMesh(false);
            UpdateWalls();
        }

        private void UpdateWalls()
        {
            int pointsCount = _pathPoints.Length;
            Vector3[] leftWallPoints = new Vector3[pointsCount];
            Vector3[] rightWallPoints = new Vector3[pointsCount];
            Vector3 left = Vector3.zero;

            for (int i = 0; i < pointsCount; i++)
            {
                Vector3 currentPoint = _pathPoints[i];
                if (i < pointsCount - 1)
                {
                    Vector3 nextPoint = _pathPoints[i + 1];
                    Vector3 forward = (nextPoint - currentPoint).normalized;
                    left = Vector3.Cross(Vector3.up, forward).normalized;
                }

                leftWallPoints[i] = currentPoint + left * (_width / 2);
                rightWallPoints[i] = currentPoint - left * (_width / 2);
            }

            Vector3 firstForward = (_pathPoints[1] - _pathPoints[0]).normalized;
            left = Vector3.Cross(Vector3.up, firstForward).normalized;
            leftWall.transform.localPosition = left * (_width / 2);
            rightWall.transform.localPosition = -left * (_width / 2);
            
            leftWall.Init(_wallWidth, _wallHeight, leftWallPoints);
            rightWall.Init(_wallWidth, _wallHeight, rightWallPoints);
        }

        public override void ConnectToNext(ProceduralMesh next)
        {
            base.ConnectToNext(next);

            if (next is RoadMesh nextRoadMesh)
            {
                Utils.CustomDebugLog(Utils.LogLevel.Info, "Trying to connect walls");
                leftWall.ConnectToNext(nextRoadMesh.leftWall);
                rightWall.ConnectToNext(nextRoadMesh.rightWall);
            }
        }
    }
}