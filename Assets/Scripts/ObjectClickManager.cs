﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ObjectClickManager : MonoBehaviour
{
    public delegate void ARHit(ARRaycastHit arRaycastHit);

    public delegate void NonARHit(RaycastHit hit);

    public static event ARHit OnARHit;
    public static event NonARHit OnNonARHit;

    private static ObjectClickManager _instance;
    public static ObjectClickManager Instance => _instance;

    [SerializeField] private ARRaycastManager arRaycastManager;
#pragma warning disable 108,114
    [SerializeField] private Camera camera;
#pragma warning restore 108,114
    public ObjectClickFilter ClickFilter { get; set; } = ObjectClickFilter.AR;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(this);
        }
        else
        {
            _instance = this;
        }
    }

    private void Update()
    {
        if (GameManager.CurrentGameState != GameManager.GameState.EditingRoad || Input.touchCount == 0)
        {
            return;
        }

        Touch touch = Input.GetTouch(0); // TODO: Maybe trigger raycast from Click Events
        if (touch.phase != TouchPhase.Began || Utils.IsPointOverUIObject(touch.position))
        {
            return;
        }

        // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Touch detected, frame: {Time.frameCount}");

        switch (ClickFilter)
        {
            case ObjectClickFilter.AR:
                ARRaycast(touch.position);
                break;
            case ObjectClickFilter.NonAR:
                NonARRaycast(touch.position);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void NonARRaycast(Vector2 touchPosition)
    {
        Ray ray = camera.ScreenPointToRay(touchPosition);
        if (Physics.Raycast(ray, out RaycastHit hitInfo))
        {
            Utils.CustomDebugLog(Utils.LogLevel.Info, $"Hit {hitInfo.collider.gameObject}");
            OnNonARHit?.Invoke(hitInfo);
        }
    }

    private void ARRaycast(Vector2 touchPosition)
    {
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        if (arRaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
        {
            string hitString = "";
            foreach (ARRaycastHit hit in hits)
            {
                hitString = $"{hitString} {hit.hitType}";
            }

            Utils.CustomDebugLog(Utils.LogLevel.Info, $"Things hit:{hitString}");
            OnARHit?.Invoke(hits[0]);
        }
    }

    public enum ObjectClickFilter
    {
        AR,
        NonAR
    }
}