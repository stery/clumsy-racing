﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARCube : MonoBehaviour
{
    public ARAnchor ARAnchor { get; set; }

    public Vector3 GetBaseScale()
    {
        // Assuming the actual cube is the first direct child of this gameobject
        Transform childCube = transform.GetChild(0);
        return childCube.localScale;
    }
}