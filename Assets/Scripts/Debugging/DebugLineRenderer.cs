﻿using UnityEngine;

namespace Debugging
{
    public sealed class DebugLineRenderer : AbstractDebugRenderer
    {
        public Color Color = Color.cyan;
        private readonly LineRenderer _lineRenderer;

        public DebugLineRenderer(GameObject source, Vector3[] points) : base(source, points)
        {
            if (!Debug.isDebugBuild) return;

            _lineRenderer = new GameObject("debug line renderer").AddComponent<LineRenderer>();
            _lineRenderer.transform.SetParent(source.transform);
            Update();
        }

        protected override void UpdateRenderer()
        {
            if (!Debug.isDebugBuild) return;
            
            _lineRenderer.startColor = Color;
            _lineRenderer.endColor = Color;
            Vector3[] points = Points;
            _lineRenderer.positionCount = points.Length;
            for (int i = 0; i < points.Length; i++)
            {
                _lineRenderer.SetPosition(i, points[i]);
            }
        }
    }
}