﻿using UnityEngine;

namespace Debugging
{
    public abstract class AbstractDebugRenderer
    {
        protected readonly GameObject Source;
        private Vector3[] _points;

        public Vector3[] Points
        {
            get => _points;
            set
            {
                _points = value;
                Update();
            }
        }
        
        protected AbstractDebugRenderer(GameObject source, Vector3[] points)
        {
            if (!Debug.isDebugBuild) return;
            
            Source = source;
            _points = points;
        }

        public void Update()
        {
            if (!Debug.isDebugBuild) return;
            
            UpdateRenderer();
        }

        protected abstract void UpdateRenderer();
    }
}