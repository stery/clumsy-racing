﻿using UnityEngine;

namespace Debugging
{
    public class DebugSphereRenderer : AbstractDebugRenderer
    {
        public Color Color = Color.cyan;
        private readonly Transform _spheresTransform;
        
        public DebugSphereRenderer(GameObject source, Vector3[] points) : base(source, points)
        {
            if (!Debug.isDebugBuild) return;

            _spheresTransform = new GameObject($"{source.name} debug sphere renderer").transform;
            // _spheresTransform.SetParent(source.transform);
            Update();
        }

        protected override void UpdateRenderer()
        {
            Vector3[] points = Points;
            int pointsCount = points.Length;
            // Add more children if needed
            for (int i = _spheresTransform.childCount; i < pointsCount; i++)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                GameObject.Destroy(sphere.GetComponent<Collider>());
                sphere.GetComponent<Renderer>().material.color = Color;
                sphere.transform.localScale = 1.5f * Vector3.one;
                sphere.transform.SetParent(_spheresTransform);
            }
            
            // Disable extra children
            for (int i = pointsCount; i < _spheresTransform.childCount; i++)
            {
                _spheresTransform.GetChild(i).gameObject.SetActive(false);
            }
            
            // Update children
            for (int i = 0; i < pointsCount; i++)
            {
                Transform sphere = _spheresTransform.GetChild(i); // TODO: Check if returns inactive as well
                sphere.gameObject.SetActive(true);
                sphere.position = points[i];
            }
        }
    }
}