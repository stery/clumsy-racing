﻿using UnityEngine;
using UnityEngine.AI;

namespace Debugging
{
    public class DebugNavMeshRenderer : MonoBehaviour
    {
        // private Color _color = new Color(0.5f, 0.25f, 0.5f);
        private readonly Color _color = Color.green;
        private NavMeshTriangulation? _triangulation;
        private Material _material;

        private void Awake()
        {
            if (!Debug.isDebugBuild)
            {
                DestroyImmediate(gameObject);
                return;
            }

            _material = new Material(Shader.Find("Standard"));
            Camera.onPostRender += OnPostRenderCallback;
        }

        public void UpdateMeshRender()
        {
            if (!Debug.isDebugBuild) return;

            _triangulation = NavMesh.CalculateTriangulation();
        }

        private void OnDestroy()
        {
            Camera.onPostRender -= OnPostRenderCallback;
        }

        private void OnPostRenderCallback(Camera camera)
        {
            if (_triangulation == null)
            {
                return;
            }

            GL.PushMatrix();
            Material material = _material;
            material.color = _color;
            material.SetPass(0);
            GL.Begin(GL.TRIANGLES);
            NavMeshTriangulation triangulation = _triangulation.Value;

            for (int i = 0; i < triangulation.indices.Length; i += 3)
            {
                int i1 = triangulation.indices[i];
                int i2 = triangulation.indices[i + 1];
                int i3 = triangulation.indices[i + 2];
                Vector3 p1 = triangulation.vertices[i1];
                Vector3 p2 = triangulation.vertices[i2];
                Vector3 p3 = triangulation.vertices[i3];
                GL.Color(_color);
                GL.Vertex(p1);
                GL.Vertex(p2);
                GL.Vertex(p3);
            }

            GL.End();
            GL.PopMatrix();
        }
    }
}