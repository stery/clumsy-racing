﻿using UnityEngine;

namespace Cars.Controllers
{
    [RequireComponent(typeof(Rigidbody))]
    public class SimpleCarController : MonoBehaviour, ICarController
    {
        private Rigidbody _rigidbody;
        private float _maxSpeed;
        private float _acceleration;
        private float _maxSteeringAngle;
        private float _forwardMovement;
        private float _steering;

        public float ForwardMovement
        {
            get => _forwardMovement;
            set => _forwardMovement = Mathf.Clamp(value, -1f, 1f);
        }
        public float Steering
        {
            get => _steering;
            set => _steering = Mathf.Clamp(value, -1f, 1f);
        }
        public float MaxSpeed => _maxSpeed;
        public float MaxSteeringAngle => _maxSteeringAngle;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.centerOfMass = Vector3.zero;
        }

        private void Start()
        {
            GameConfiguration gameConfiguration = DependenciesManager.GameConfiguration;
            _maxSpeed = gameConfiguration.MAXSpeed;
            _acceleration = gameConfiguration.Acceleration;
            _maxSteeringAngle = gameConfiguration.MAXSteeringAngle;
        }

        private void FixedUpdate()
        {
            ApplyForwardMovement();
            ApplySteering();
        }

        private void ApplyForwardMovement()
        {
            if (Mathf.Abs(_forwardMovement) < 0.1f)
            {
                return;
            }

            Vector3 force = transform.forward * (_acceleration * _forwardMovement);
            // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Force added: {force}");

            _rigidbody.AddForce(force, ForceMode.Acceleration);
            if (_rigidbody.velocity.magnitude > _maxSpeed)
            {
                _rigidbody.velocity = _rigidbody.velocity.normalized * _maxSpeed;
            }
        }

        private void ApplySteering()
        {
            if (Mathf.Abs(Steering) < 0.1f)
            {
                return;
            }

            Vector3 relativeVelocity = transform.InverseTransformDirection(_rigidbody.velocity);
            float forwardMovement = Mathf.Clamp(relativeVelocity.z, -1f, 1f);
            Vector3 torque = transform.up * (_steering * _maxSteeringAngle * forwardMovement);
            // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Torque added: {torque}");
            _rigidbody.AddTorque(torque, ForceMode.Acceleration);
        }
    }
}