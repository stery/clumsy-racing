﻿using System;
using System.Collections.Generic;
using System.Linq;
using Segments.ControlPoints;
using UnityEngine;
using UnityEngine.AI;

namespace Cars.Drivers
{
    public class NavMeshDataProvider : INavigationDataProvider
    {
        private readonly ControlPointStore _controlPointStore;
        private readonly NavMeshSurface _navMeshSurface;
        private List<Vector3> _navWaypoints;

        public NavMeshDataProvider(ControlPointStore controlPointStore, NavMeshSurface navMeshSurface)
        {
            _controlPointStore = controlPointStore;
            _navMeshSurface = navMeshSurface;

            GameManager.OnGameStateChanged += OnGameStateChanged;
        }

        ~NavMeshDataProvider()
        {
            GameManager.OnGameStateChanged -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameManager.GameState gameState)
        {
            if (gameState == GameManager.GameState.Playing)
            {
                BakeNavMesh();
            }
        }

        private void BakeNavMesh()
        {
            // Bake navmesh
            NavMesh.RemoveAllNavMeshData();
            _navMeshSurface.BuildNavMesh();
            // FindObjectOfType<DebugNavMeshRenderer>()?.UpdateMeshRender();
            
            // Bake navmesh path for bots
            _navWaypoints = new List<Vector3>();
            List<Vector3> controlPoints = GetCheckpoints();
            int controlPointsCount = controlPoints.Count;
            NavMeshPath navMeshPath = new NavMeshPath();
            for (int i = 0; i < controlPointsCount; i++)
            {
                int iNext = (i + 1) % controlPointsCount;
                Vector3 currentCp = controlPoints[i];
                Vector3 nextCp = controlPoints[iNext];
                NavMesh.CalculatePath(currentCp, nextCp, 1 << 0, navMeshPath);
                _navWaypoints.AddRange(navMeshPath.corners);
            }
            
            _navWaypoints = _navWaypoints.Distinct().ToList();

            if (_navWaypoints.Count == 0)
            {
                throw new InvalidProgramException(
                    "Could not generate waypoints. Is the path wide enough for the agent?");
            }
        }

        public List<Vector3> GetCheckpoints()
        {
            return _controlPointStore.ControlPoints
                .Select(point => point.transform.position)
                .ToList();
        }

        public List<Vector3> GetIntermediateWaypoints()
        {
            return _navWaypoints.ToList();
        }
    }
}