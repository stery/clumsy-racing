﻿using Cars.Controllers;
using UnityEngine;

namespace Cars.Drivers
{
    [RequireComponent(typeof(SimpleCarController))]
    public abstract class AbstractCarDriver : MonoBehaviour
    {
        protected SimpleCarController CarController { get; private set; }

        private UpdateStrategy _updateStrategy;

        private void Awake()
        {
            CarController = GetComponent<SimpleCarController>();
            _updateStrategy = GetUpdateStrategy();

            AwakeExtra();
        }

        private void Update()
        {
            if (_updateStrategy != UpdateStrategy.Update)
            {
                return;
            }
            
            MoveCar();
            UpdateExtra();
        }

        private void FixedUpdate()
        {
            if (_updateStrategy != UpdateStrategy.FixedUpdate)
            {
                return;
            }
            
            MoveCar();
            FixedUpdateExtra();
        }

        protected virtual void AwakeExtra()
        {
        }

        protected virtual void UpdateExtra()
        {
        }

        protected virtual void FixedUpdateExtra()
        {
        }

        protected abstract void MoveCar();
        protected abstract UpdateStrategy GetUpdateStrategy();
        
        protected enum UpdateStrategy
        {
            FixedUpdate, Update
        }
    }
}