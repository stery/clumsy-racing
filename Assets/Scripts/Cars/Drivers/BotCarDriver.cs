﻿using System.Collections.Generic;
using Cars.Controllers;
using Debugging;
using UnityEngine;

namespace Cars.Drivers
{
    [RequireComponent(typeof(SimpleCarController))]
    public sealed class BotCarDriver : AbstractCarDriver
    {
        private const float UNSTUCK_TIMEOUT = 3f;

        [SerializeField] private float distanceThreshold;
        [SerializeField] private float lowAngleSteeringSpeed;
        [SerializeField] private float highAngleSteeringSpeed;

        private SimpleCarController _carController;
        private List<Vector3> _checkpoints;
        private List<Vector3> _path;
        private int _currentPathId;

        // Unstuck vars
        private float _deltaDistance;
        private Vector3 _lastPos;
        private float _lastChecked;
        private bool _isStuck;

        // Debugging
        private DebugLineRenderer _debugPathLineRenderer;
        private DebugSphereRenderer _debugPathSphereRenderer;
        private DebugSphereRenderer _debugWaypointsRenderer;

        private Vector3 TargetPos
        {
            get
            {
                // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Accessing target pos at {_currentPathId}/{_path.Count}");
                return _path[_currentPathId];
            }
        }

        protected override UpdateStrategy GetUpdateStrategy()
        {
            return UpdateStrategy.FixedUpdate;
        }

        private void Awake()
        {
            _carController = GetComponent<SimpleCarController>();
        }

        private void OnEnable()
        {
            _checkpoints = DependenciesManager.NavigationDataProvider.GetCheckpoints();
            _path = DependenciesManager.NavigationDataProvider.GetIntermediateWaypoints();

            _debugPathLineRenderer = new DebugLineRenderer(gameObject, _path.ToArray());
            _debugPathSphereRenderer = new DebugSphereRenderer(gameObject, _path.ToArray());
            _debugWaypointsRenderer = new DebugSphereRenderer(gameObject, _checkpoints.ToArray())
            {
                Color = Color.magenta, Points = _checkpoints.ToArray()
            };
            _debugWaypointsRenderer.Update();
            FindObjectOfType<DebugNavMeshRenderer>()?.UpdateMeshRender();
        }

        protected override void MoveCar()
        {
            if (!_isStuck && _currentPathId < _path.Count)
            {
                MoveToTargetPos();
                HandleTargetPosChange();
            }

            MonitorStucking();
        }

        private void MoveToTargetPos()
        {
            Vector3 target = transform.InverseTransformPoint(TargetPos);
            Quaternion targetRotation = Quaternion.LookRotation(target, transform.up);
            float steeringAngle = targetRotation.eulerAngles.y;
            steeringAngle = Utils.Euler360AngleToNegativePositive180(steeringAngle);
            // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Target angle is {steeringAngle}. " +
            //           $"path id: {_currentPathId}/{_path.Count}.");
            /*
         * Map angle "difficulty" to speed:
         * [MaxSteeringAngle/2, MaxSteeringAngle] -> [1, 0.4]
         * This results in faster cornering for lower angles
         * and more precautious cornering for higher angles.
         */
            float maxSteeringAngle = _carController.MaxSteeringAngle;
            float clampedAngle = Mathf.Clamp(Mathf.Abs(steeringAngle), maxSteeringAngle / 2, maxSteeringAngle);
            float forwardMovement = Utils.Rescale(clampedAngle,
                maxSteeringAngle / 2, maxSteeringAngle,
                lowAngleSteeringSpeed, highAngleSteeringSpeed);
            // float steering = steeringAngle / 180;
            float steering = Mathf.Abs(steeringAngle) < 2f ? 0f : Mathf.Sign(steeringAngle);

            _carController.ForwardMovement = forwardMovement;
            _carController.Steering = steering;
        }

        private void HandleTargetPosChange()
        {
            float distance = Vector3.Distance(transform.position, TargetPos);
            // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Distance to {TargetPos} is {distance}");

            if (distance < distanceThreshold)
            {
                _currentPathId = (_currentPathId + 1) % _path.Count;
                // Utils.CustomDebugLog(Utils.LogLevel.Info, $"Changed path id to {_currentPathId}: {TargetPos}");
            }
        }

        private void MonitorStucking()
        {
            float time = Time.time;
            _deltaDistance += (_lastPos - transform.position).sqrMagnitude;
            _lastPos = transform.position;

            if (time - _lastChecked > UNSTUCK_TIMEOUT)
            {
                _lastChecked = time;
                if (_isStuck)
                {
                    _carController.ForwardMovement = 0f;
                    // TODO(performance): Use squared distance instead of distance
                    float minDistance = Vector3.Distance(transform.position, _path[_currentPathId]);
                    int closestId = _currentPathId;
                    int pathPointsCount = _path.Count;
                    for (int i = _currentPathId; i != _currentPathId; i = (i + 1) % pathPointsCount)
                    {
                        float currentDistance = Vector3.Distance(transform.position, _path[i]);
                        if (currentDistance < minDistance)
                        {
                            minDistance = currentDistance;
                            closestId = i;
                        }
                    }

                    _currentPathId = closestId;
                    Utils.CustomDebugLog(Utils.LogLevel.Info, $"Closest point seems to be {_currentPathId}/{pathPointsCount} " +
                              $"at {_path[_currentPathId]} (distance: {minDistance})");

                    Utils.CustomDebugLog(Utils.LogLevel.Info, "Should be unstucked");
                    _isStuck = false;
                    return;
                }

                if (_deltaDistance < 2f)
                {
                    /*
                 * TODO(unstuck): Better unstucking idea
                 * Compute path to closest point (or simply use it without path),
                 * drive backwards and steer so that the angle between forward
                 * and the point is less than 90 degrees,
                 * then the movement should be able to pick it up.
                 */
                    // Apply reverse to help unstucking
                    _carController.ForwardMovement = -1f;
                    _carController.Steering = 0f;
                    _carController.transform.localPosition += Vector3.up;
                    Utils.CustomDebugLog(Utils.LogLevel.Info, "Trying to unstuck");
                    _isStuck = true;
                }

                _deltaDistance = 0;
            }
        }
    }
}