﻿using InputProviders;

namespace Cars.Drivers
{
    public sealed class PlayerCarDriver : AbstractCarDriver
    {
        private IInputProvider _inputProvider;

        protected override UpdateStrategy GetUpdateStrategy()
        {
            return UpdateStrategy.Update;
        }

        protected override void AwakeExtra()
        {
            base.AwakeExtra();
            _inputProvider = DependenciesManager.InputProvider;
        }

        protected override void MoveCar()
        {
            float forward = _inputProvider.GetForwardInput();
            float steering = _inputProvider.GetSteeringInput();

            CarController.ForwardMovement = forward;
            CarController.Steering = steering;
        }
    }
}