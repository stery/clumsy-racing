﻿using System.Collections.Generic;
using UnityEngine;

namespace Cars.Drivers
{
    public interface INavigationDataProvider
    {
        public List<Vector3> GetCheckpoints();
        public List<Vector3> GetIntermediateWaypoints();
    }
}