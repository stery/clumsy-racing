﻿using System;
using UnityEngine;

namespace Cars.Deformation
{
    [RequireComponent(typeof(Collider))]
    public class DeformablePart : MonoBehaviour
    {
        [SerializeField] private SkinnedMeshRenderer skinnedMeshRenderer;
        [SerializeField] private string blendShapeName;
        [SerializeField] private int blendShapeId;
        [SerializeField] private int maxDamage = 100;
        [SerializeField] private int baseDamage = 20;

        private int _damage;

        private int Damage
        {
            get => _damage;
            set
            {
                _damage = Mathf.Clamp(value, 0, maxDamage);
                Utils.CustomDebugLog(Utils.LogLevel.Info, $"Taking damage on {blendShapeName}, {_damage}/{maxDamage}");
                UpdateMeshBlend();
            }
        }

        private void Start()
        {
            CheckFieldsInitialized();
        }

        public void Deform()
        {
            Damage += baseDamage;
        }

        private void UpdateMeshBlend()
        {
            skinnedMeshRenderer.SetBlendShapeWeight(blendShapeId, Damage / (float) maxDamage * 100);
        }

        private void CheckFieldsInitialized()
        {
            if (skinnedMeshRenderer == null)
            {
                throw new ArgumentException("Skinned Mesh Renderer is not set");
            }

            if (String.IsNullOrWhiteSpace(blendShapeName))
            {
                throw new ArgumentException("Blend shape name is not set");
            }

            blendShapeId = skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex(blendShapeName);
            if (blendShapeId < 0)
            {
                throw new ArgumentException("Blend shape name is invalid");
            }

            if (GetComponentInParent<MainDeformer>() == null)
            {
                throw new ArgumentException("Main Deformer was not found up in the hierarchy. " +
                                            "Make sure there is a game object with a rigidbody also having " +
                                            "Main Deformer attached.");
            }
        }
    }
}