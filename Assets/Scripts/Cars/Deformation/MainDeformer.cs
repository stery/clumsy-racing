﻿using UnityEngine;

namespace Cars.Deformation
{
    /// <summary>
    /// Script to be used on the parent of deformable parts mesh containing the rigidbody
    /// </summary>
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    public class MainDeformer : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            if (other.collider.CompareTag(tag)) return;

            foreach (ContactPoint contact in other.contacts)
            {
                DeformablePart deformablePart = contact.thisCollider.GetComponent<DeformablePart>();

                if (deformablePart != null)
                {
                    deformablePart.Deform();

                    return; // Save some time. Is it safe though?
                }
            }
        }
    }
}