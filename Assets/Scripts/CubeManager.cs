using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class CubeManager : MonoBehaviour
{
    public delegate void CubeCreated(ARCube arCube);

    public delegate void CubeDestroyed(ARCube arCube);

    public static event CubeCreated OnCubeCreated;
    public static event CubeDestroyed OnCubeDestroyed;
    
    public GameObject cubePrefab;

    private static CubeManager _instance;
    public static CubeManager Instance => _instance;
    
    [SerializeField] private ARAnchorManager arAnchorManager;
    [SerializeField] private ARPlaneManager arPlaneManager;
    private List<ARAnchor> _anchors;

    private bool _adding = true;
    private int _count = 0;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(this);
        }
        else
        {
            _instance = this;
        }
        _anchors = new List<ARAnchor>();
    }

    private void Start()
    {
        ObjectClickManager.OnARHit += AddCube;
        ObjectClickManager.OnNonARHit += RemoveCube;
    }

    private void AddCube(ARRaycastHit hit)
    {
        Pose hitPose = hit.pose;
        TrackableId trackableId = hit.trackableId;
        ARPlane hitPlane = arPlaneManager.GetPlane(trackableId);

        ARAnchor anchor = arAnchorManager.AttachAnchor(hitPlane, hitPose);

        if (ReferenceEquals(anchor, null))
        {
            Utils.CustomDebugLog(Utils.LogLevel.Warn, "Error creating anchor");
        }
        else
        {
            GameObject cube = Instantiate(cubePrefab, anchor.transform);
            ARCube arCube = cube.GetComponent<ARCube>();
            arCube.ARAnchor = anchor;
            _anchors.Add(anchor);
            _count++;
            OnCubeCreated?.Invoke(arCube);
        }
    }

    private void RemoveCube(RaycastHit hit)
    {
        GameObject cube = hit.collider.gameObject;
        ARCube arCube = cube.GetComponent<ARCube>() ?? cube.transform.parent.GetComponent<ARCube>();
        if (ReferenceEquals(arCube, null))
        {
            Utils.CustomDebugLog(Utils.LogLevel.Info, "Object hit is not an ARCube");
            return;
        }

        ARAnchor anchor = arCube.ARAnchor;
        OnCubeDestroyed?.Invoke(arCube);
        arAnchorManager.RemoveAnchor(anchor); // This should also destroy the cube
        _count--;
        _anchors.Remove(anchor);
    }
}
