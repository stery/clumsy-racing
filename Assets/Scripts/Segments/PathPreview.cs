﻿using System.Linq;
using UnityEngine;

namespace Segments
{
    public class PathPreview : MonoBehaviour
    {
        public Color lineColor = new Color(0f, 0.7f, 0f);
        public float animationSpeed = 0.25f;

        private Vector3[] _pathPoints;
        private LineRenderer _lineRenderer;
        private Material _dashedLineMaterial;

        public void SetPathPoints(Vector3[] pathPoints)
        {
            _pathPoints = pathPoints.Select(point => point + Vector3.up * 3).ToArray();
            UpdatePathPreview();
        }
        
        private void Start()
        {
            _lineRenderer = gameObject.AddComponent<LineRenderer>();
            _dashedLineMaterial = Resources.Load<Material>("DashedLine");
            _dashedLineMaterial.mainTextureScale = new Vector2(0.1f, 1f);
            _lineRenderer.material = _dashedLineMaterial;
            _dashedLineMaterial = _lineRenderer.material; // Makes a copy, preventing editing the actual on-disk material
            _lineRenderer.textureMode = LineTextureMode.Tile;
            Utils.CustomDebugLog(Utils.LogLevel.Info, "PathPreview started");
        }

        private void Update()
        {
            Vector2 offset = _dashedLineMaterial.mainTextureOffset;
            offset.x = Mathf.Repeat(offset.x - animationSpeed * Time.deltaTime, 1f);
            _dashedLineMaterial.mainTextureOffset = offset;
        }

        private void UpdatePathPreview()
        {
            _lineRenderer.startColor = lineColor;
            _lineRenderer.endColor = lineColor;
            _lineRenderer.widthMultiplier = 3;

            _lineRenderer.positionCount = _pathPoints.Length; // Is this required?
            _lineRenderer.SetPositions(_pathPoints);
        }
    }
}