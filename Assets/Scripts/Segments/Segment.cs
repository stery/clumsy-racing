﻿using System;
using System.Collections.Generic;
using Meshes;
using Meshes.RoadSide;
using Segments.ControlPoints;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Segments
{
    public class Segment : MonoBehaviour
    {
        public ControlPoint From { get; private set; }
        public ControlPoint To { get; private set; }
        public Vector3[] PathPoints { get; private set; }
        private RoadMesh _roadMesh;
        // Extras like road-side objects
        private RoadSideMesh _roadSideMesh;
        private bool _initialized;

        private Color? _gizmoColor;

        public void UpdatePath(Vector3[] pathPoints)
        {
            if (!_initialized)
            {
                throw new InvalidOperationException("Segment must be initialized before updating path");
            }

            PathPoints = pathPoints;
            transform.position = From.transform.position;

            // Triggers mesh reconfiguration
            _roadMesh.PathPoints = PathPoints;
            _roadSideMesh.PathPoints = PathPoints;
            _roadSideMesh.UpdateRoadBounds(_roadMesh.Bounds);
        }

        public void Init(ControlPoint from, ControlPoint to, Vector3[] pathPoints)
        {
            From = from;
            To = to;
            PathPoints = pathPoints;

            transform.position = From.transform.position; // TODO: Add height offset?

            CreateRoadMesh();
            CreateRoadSideMesh();

            _initialized = true;
        }

        public void SpawnRoadSideExtras()
        {
            if (!_initialized)
            {
                throw new InvalidOperationException("Segment must be initialized before spawning extras");
            }

            _roadSideMesh.SpawnExtras();
        }

        private void CreateRoadMesh()
        {
            GameConfiguration gameConfiguration = DependenciesManager.GameConfiguration;

            _roadMesh = Instantiate(gameConfiguration.RoadMeshPrefab, transform, false);
            // _roadMesh.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);

            _roadMesh.Init(gameConfiguration.RoadWidth, gameConfiguration.RoadHeight, PathPoints,
                gameConfiguration.RoadWallWidth, gameConfiguration.RoadWallHeight);

            _roadMesh.transform.localPosition += Vector3.up * 0.2f; // Add height from ground/roadside mesh
        }

        private void CreateRoadSideMesh()
        {
            GameConfiguration gameConfiguration = DependenciesManager.GameConfiguration;
            List<RoadSideData> roadSideData = gameConfiguration.RoadSideData;
            RoadSideData data = roadSideData[Random.Range(0, roadSideData.Count)];

            _roadSideMesh = new GameObject($"RoadSide-{data.type}").AddComponent<RoadSideMesh>();
            _roadSideMesh.transform.SetParent(transform, false);

            _roadSideMesh.Init(gameConfiguration.RoadSideWidth, 0.01f, PathPoints, data, gameConfiguration.RoadSideExtrasRetryCount, _roadMesh.Bounds);
        }

        public void ConnectToNext(Segment next)
        {
            _roadMesh.ConnectToNext(next._roadMesh);
            _roadSideMesh.ConnectToNext(next._roadSideMesh);
        }

        private void OnDrawGizmos()
        {
            _gizmoColor ??= Utils.GetRandomColor();

            Gizmos.color = _gizmoColor.Value;
            Vector3 position = transform.position + Vector3.up * 2f;

            for (int point = 0; point < PathPoints.Length; point++)
            {
                Vector3 pathPoint = PathPoints[point];
                Gizmos.DrawSphere(pathPoint + position, 0.5f);

                // Handles.Label(pathPoint + position + Vector3.up, $"{point}");
            }
        }
    }
}