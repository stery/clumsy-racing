﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Segments.ControlPoints;
using UnityEngine;

namespace Segments
{
    public class SegmentGenerator
    {
        private readonly SegmentStore _segmentStore;
        private readonly ControlPointStore _controlPointStore;
        private readonly int _hermiteDivisionsCount;
        private readonly int _hillPeaksCount;

        public SegmentGenerator(SegmentStore segmentStore,
                                ControlPointStore controlPointStore,
                                int hermiteDivisionsCount, int hillPeaksCount)
        {
            _segmentStore = segmentStore;
            _controlPointStore = controlPointStore;
            _hermiteDivisionsCount = hermiteDivisionsCount;
            _hillPeaksCount = hillPeaksCount;

            AbstractControlPointGenerator.OnControlPointGenerated += OnControlPointGenerated;
            GameManager.OnGameStateChanged += OnGameStateChanged;
        }

        ~SegmentGenerator()
        {
            Destroy();
        }

        public void Destroy()
        {
            AbstractControlPointGenerator.OnControlPointGenerated -= OnControlPointGenerated;
            GameManager.OnGameStateChanged -= OnGameStateChanged;
        }

        private void OnControlPointGenerated(ControlPoint controlPoint)
        {
            if (_controlPointStore.Count < 2)
            {
                return;
            }

            CreateSegmentWithoutNext();

            int segmentsCount = _segmentStore.Count;
            if (segmentsCount > 2)
            {
                // The first one should be recomputed when switching to play mode
                RecomputePathForSegment(segmentsCount - 2);
            }
        }
        
        private void OnGameStateChanged(GameManager.GameState gameState)
        {
            if (gameState == GameManager.GameState.Playing)
            {
                CloseLoop();
            }
            else
            {
                _segmentStore.DestroyAll();
            }
        }

        public void CloseLoop()
        {
            // 1. Create last segment, with Next (because it's known, it's the first CP)\
            IReadOnlyList<ControlPoint> controlPoints = _controlPointStore.ControlPoints;
            int controlPointsCount = controlPoints.Count;

            ControlPoint[] lastSegmentCPs = ComputeControlPointsForSegmentStartingFrom(controlPointsCount - 1);
            CreateSegment(lastSegmentCPs, true);

            /*
             * 2. All paths must be recomputed because
             * a Control Point's position might get changed during road creation
             * and so the beginning of a new segment can be different from the
             * end of the previous segment if the CP connecting them was
             * recalibrated in the meantime, therefore displacements can appear
             */
            ReadOnlyCollection<Segment> segments = _segmentStore.Segments;
            int segmentsCount = segments.Count;
            Segment prevSegment = segments[segmentsCount - 1];
            for (int segmentId = 0; segmentId < segmentsCount; ++segmentId)
            {
                RecomputePathForSegment(segmentId);
                
                // 3. Connect
                Segment currentSegment = segments[segmentId];
                prevSegment.ConnectToNext(currentSegment);
                prevSegment = currentSegment;
                
                // 4. Spawn/Reposition roadside extras
                currentSegment.SpawnRoadSideExtras();
            }
            
            // Reconnect last segment because it gets recomputed *after* connect
            prevSegment.ConnectToNext(segments[0]);
        }

        private void DestroyAllSegments()
        {
            _segmentStore.DestroyAll();
        }

        public void RecreateAll()
        {
            DestroyAllSegments();
            CreateAll();
        }

        private void RecomputePathForSegment(int segmentId)
        {
            Segment segment = _segmentStore.Segments[segmentId];

            ControlPoint[] controlPoints = ComputeControlPointsForSegmentStartingFrom(segmentId);
            Vector3[] controlPointPositions = controlPoints
                .Select(cp => cp.transform.position)
                .ToArray();

            List<Vector3> hermitePoints = Utils.GetHermitePoints(
                controlPointPositions[0],
                controlPointPositions[1],
                controlPointPositions[2],
                controlPointPositions[3],
                _hermiteDivisionsCount);
            List<Vector3> randomWalkedHermitePoints = Utils.HermiteWithRandomWalkHeight(
                controlPointPositions[0],
                controlPointPositions[1],
                controlPointPositions[2],
                controlPointPositions[3],
                _hermiteDivisionsCount, _hillPeaksCount, 1f);

            Vector3[] path = Utils.BlendPointsEnds(
                    hermitePoints, randomWalkedHermitePoints,
                    Math.Max(3, _hermiteDivisionsCount / 10))
                .ToArray();

            segment.UpdatePath(path);
        }

        private void CreateWithNext()
        {
            IReadOnlyList<ControlPoint> controlPoints = _controlPointStore.ControlPoints;

            int controlPointsCount = controlPoints.Count;
            int i = Mathf.Max(0, controlPointsCount - 1); // TODO: Check this id, could be -2
            int prev = i - 1 < 0 ? controlPointsCount - 1 : i - 1;
            int p0 = i;
            int p1 = (i + 1) % controlPointsCount;
            int next = (i + 2) % controlPointsCount;

            ControlPoint[] segmentControlPoints =
            {
                controlPoints[prev],
                controlPoints[p0],
                controlPoints[p1],
                controlPoints[next]
            };

            CreateSegment(segmentControlPoints, true);
            // CreateSegment(controlPoints.Skip(count).Concat(controlPoints.Take(2)).ToArray(), true);
        }

        private void CreateSegmentWithoutNext()
        {
            IReadOnlyList<ControlPoint> controlPoints = _controlPointStore.ControlPoints;
            int controlPointsCount = controlPoints.Count;

            if (controlPointsCount < 2)
            {
                throw new InvalidOperationException("Can't generate segment with less " +
                                                    $"than 2 control points (actual: {controlPointsCount})");
            }

            int p0 = controlPointsCount - 2;
            int prev = p0 - 1 < 0 ? controlPointsCount - 1 : p0 - 1;
            int p1 = (p0 + 1) % controlPointsCount;
            ControlPoint[] segmentControlPoints = {controlPoints[prev], controlPoints[p0], controlPoints[p1]};

            CreateSegment(segmentControlPoints, false);
        }

        private void CreateSegment(IReadOnlyList<ControlPoint> controlPoints, bool withNext)
        {
            int controlPointsCount = controlPoints.Count;

            if (!withNext && controlPointsCount != 3 || withNext && controlPointsCount != 4)
            {
                throw new ArgumentException($"Wrong number of control points provided: {controlPointsCount}, " +
                                            $"expected {(withNext ? 4 : 3)}");
            }

            Vector3 prevPoint = controlPoints[0].transform.position;
            Vector3 p0Point = controlPoints[1].transform.position;
            Vector3 p1Point = controlPoints[2].transform.position;
            Vector3 nextPoint = withNext
                ? controlPoints[3].transform.position
                : p1Point + (p1Point - p0Point).normalized;

            List<Vector3> hermitePoints = Utils.GetHermitePoints(
                prevPoint,
                p0Point,
                p1Point,
                nextPoint,
                _hermiteDivisionsCount);
            List<Vector3> randomWalkedHermitePoints = Utils.HermiteWithRandomWalkHeight(
                prevPoint,
                p0Point,
                p1Point,
                nextPoint,
                _hermiteDivisionsCount, _hillPeaksCount, 1f);

            Vector3[] points = Utils.BlendPointsEnds(
                    hermitePoints, randomWalkedHermitePoints,
                    Math.Max(3, _hermiteDivisionsCount / 10))
                .ToArray();

            Segment segment = Generate(controlPoints[1], controlPoints[2], points);
            segment.SpawnRoadSideExtras();
        }

        private void CreateAll()
        {
            IReadOnlyList<ControlPoint> controlPoints = _controlPointStore.ControlPoints;
            int controlPointsCount = controlPoints.Count;

            for (int i = 0; i < controlPointsCount; i++)
            {
                ControlPoint[] segmentControlPoints = ComputeControlPointsForSegmentStartingFrom(i);
                
                CreateSegment(segmentControlPoints, true);
            }
        }

        private Segment Generate(ControlPoint from, ControlPoint to, Vector3[] pathPoints)
        {
            Segment segment = new GameObject($"Segment{_segmentStore.Count}").AddComponent<Segment>();
            segment.Init(from, to, pathPoints);

            _segmentStore.AddSegment(segment);

            return segment;
        }

        private ControlPoint[] ComputeControlPointsForSegmentStartingFrom(int controlPointId)
        {
            // Assuming #CPs = #segments + 1
            IReadOnlyList<ControlPoint> controlPoints = _controlPointStore.ControlPoints;
            int controlPointsCount = controlPoints.Count;

            int prev = controlPointId - 1 < 0 ? controlPointsCount - 1 : controlPointId - 1;
            int p0 = controlPointId;
            int p1 = (controlPointId + 1) % controlPointsCount;
            int next = (controlPointId + 2) % controlPointsCount;
            Utils.CustomDebugLog(Utils.LogLevel.Info, $"Indices: p-1: {prev}, p0: {p0}, p1: {p1}, p2: {next}");

            return new[] {
                controlPoints[prev], controlPoints[p0],
                controlPoints[p1], controlPoints[next]
            };
        }
    }
}