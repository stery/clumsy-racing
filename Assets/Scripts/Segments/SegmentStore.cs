﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Segments
{
    // MonoBehaviour so it holds the segments under its game object
    // Can be extended for AR so all segments are anchored in a single point (this game object)
    public class SegmentStore : MonoBehaviour
    {
        private List<Segment> _segments;

        public int Count => _segments.Count;
        public ReadOnlyCollection<Segment> Segments => _segments.AsReadOnly();

        private void Awake()
        {
            _segments = new List<Segment>();
        }

        public virtual void AddSegment(Segment segment)
        {
            segment.transform.SetParent(transform, true);
            _segments.Add(segment);

            // AR implementation should remove anchor
        }

        public void DestroyAll()
        {
            _segments.ForEach(segment => Destroy(segment.gameObject));
            _segments.Clear();
        }

        private void ConnectAllSegments()
        {
            int count = _segments.Count;
            Segment last = _segments[count - 1];

            for (int i = 0; i < count; ++i)
            {
                Segment current = _segments[i];
                last.ConnectToNext(current);
                last = current;
            }
        }
    }
}