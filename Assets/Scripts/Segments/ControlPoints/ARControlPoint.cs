﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Segments.ControlPoints
{
    [RequireComponent(typeof(ARAnchor))]
    public sealed class ARControlPoint : ControlPoint
    {
        public ARAnchor Anchor { get; private set; }

        private void Awake()
        {
            Anchor = GetComponent<ARAnchor>();
        }
    }
}