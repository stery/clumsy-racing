﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Segments.ControlPoints
{
    public class ControlPointStore
    {
        private readonly List<ControlPoint> _controlPoints;

        public int Count => _controlPoints.Count;
        public ControlPoint First => _controlPoints[0];
        public ControlPoint Last => _controlPoints[Count - 1];
        public IReadOnlyList<ControlPoint> ControlPoints => _controlPoints.AsReadOnly();

        public ControlPointStore()
        {
            _controlPoints = new List<ControlPoint>();

            GameManager.OnGameStateChanged += OnGameStateChanged;
        }

        ~ControlPointStore()
        {
            GameManager.OnGameStateChanged -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameManager.GameState gameState)
        {
            if (gameState == GameManager.GameState.EditingRoad)
            {
                _controlPoints.ForEach(point => Object.Destroy(point.gameObject));
                _controlPoints.Clear();
            }
        }

        public void AddControlPoint(ControlPoint controlPoint)
        {
            _controlPoints.Add(controlPoint);

            IReadOnlyList<Vector3> previewControlPoints =
                GetControlPointsForSegmentStartingFrom(Count - 1)
                    .Select(cp => cp.transform.position)
                    .ToList();
            List<Vector3> segmentPath = Utils.GetHermitePoints(
                previewControlPoints[0], previewControlPoints[1],
                previewControlPoints[2], previewControlPoints[3],
                DependenciesManager.GameConfiguration.HermiteDivisionsCount);
            DependenciesManager.LastSegmentPreview.SetPathPoints(segmentPath.ToArray());
        }

        public void InsertControlPoint(int index, ControlPoint controlPoint)
        {
            _controlPoints.Insert(index, controlPoint);
        }

        public ControlPoint FindBefore(ControlPoint controlPoint)
        {
            int index = _controlPoints.IndexOf(controlPoint);

            if (index == -1)
            {
                throw new InvalidOperationException("Provided control point does not exist in store");
            }

            int beforeIndex = index - 1;
            if (beforeIndex == -1)
            {
                beforeIndex = Count - 1;
            }

            return _controlPoints[beforeIndex];
        }

        public IReadOnlyList<ControlPoint> GetControlPointsForSegmentStartingFrom(int startIndex)
        {
            int controlPointsCount = Count;
            int prev = startIndex - 1 < 0 ? controlPointsCount - 1 : startIndex - 1;
            int p0 = startIndex;
            int p1 = (startIndex + 1) % controlPointsCount;
            int next = (startIndex + 2) % controlPointsCount;

            return new[]
            {
                _controlPoints[prev],
                _controlPoints[p0],
                _controlPoints[p1],
                _controlPoints[next]
            };
        }
    }
}