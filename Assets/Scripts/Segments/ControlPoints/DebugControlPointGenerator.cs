﻿using InputProviders;
using UnityEngine;

namespace Segments.ControlPoints
{
    public class DebugControlPointGenerator : AbstractControlPointGenerator
    {
        protected override ControlPoint Generate(Vector3 position)
        {
            ControlPoint controlPoint = Object.Instantiate(ControlPointPrefab, position, Quaternion.identity);

            return controlPoint;
        }

        public DebugControlPointGenerator(IInputProvider inputProvider,
            ControlPointStore store,
            ControlPoint controlPointPrefab) : base(inputProvider, store, controlPointPrefab)
        {
            InputProvider.OnClickEvent += OnClick;
        }

        public override void Destroy()
        {
            base.Destroy();
            InputProvider.OnClickEvent -= OnClick;
        }

        private void OnClick(Vector2 cursorPosition)
        {
            Ray ray = Camera.main.ScreenPointToRay(cursorPosition);
            if (Physics.Raycast(ray, out RaycastHit hit, 1000f))
            {
                Utils.CustomDebugLog(Utils.LogLevel.Info, $"Hit {hit.collider.gameObject} at {hit.point}");
                Create(hit.point);
            }
        }
    }
}