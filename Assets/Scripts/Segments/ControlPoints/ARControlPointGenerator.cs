﻿using System.Collections.Generic;
using AR;
using InputProviders;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace Segments.ControlPoints
{
    public class ARControlPointGenerator : AbstractControlPointGenerator
    {
        private readonly IARManager _arManager;

        public ARControlPointGenerator(IInputProvider inputProvider,
            ControlPointStore store, ControlPoint controlPointPrefab,
            IARManager arManager) : base(inputProvider, store, controlPointPrefab)
        {
            _arManager = arManager;

            InputProvider.OnClickEvent += OnClick;
        }

        public override void Destroy()
        {
            base.Destroy();
            InputProvider.OnClickEvent -= OnClick;
        }

        private void OnClick(Vector2 cursorPosition)
        {
            if (Utils.IsPointOverUIObject(cursorPosition))
            {
                 Utils.CustomDebugLog(Utils.LogLevel.Info, "Cursor clicked over UI object");
                 return;
            }
            
            ARRaycast(cursorPosition);
        }

        protected override ControlPoint Generate(Vector3 position)
        {
            ControlPoint controlPoint = Object.Instantiate(ControlPointPrefab, position, Quaternion.identity);
            GameObject controlPointGO = controlPoint.gameObject;

            Object.Destroy(controlPoint);
            // ARAnchor anchor = controlPointGO.AddComponent<ARAnchor>();
            ARControlPoint arControlPoint = controlPointGO.AddComponent<ARControlPoint>();

            return arControlPoint;
        }

        private void ARRaycast(Vector2 touchPosition)
        {
            List<ARRaycastHit> hits = new List<ARRaycastHit>();
            if (_arManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
            {
                string hitString = "";
                foreach (ARRaycastHit hit in hits)
                {
                    hitString = $"{hitString} {hit.hitType}";
                }

                Utils.CustomDebugLog(Utils.LogLevel.Info, $"Things hit:{hitString}");

                Create(hits[0].pose.position);
            }
        }
    }
}