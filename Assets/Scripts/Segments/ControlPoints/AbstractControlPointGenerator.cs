﻿using System;
using InputProviders;
using UnityEngine;

namespace Segments.ControlPoints
{
    public abstract class AbstractControlPointGenerator
    {
        public static event Action<ControlPoint> OnControlPointGenerated;

        private ControlPointStore _store;
        protected readonly IInputProvider InputProvider;
        protected readonly ControlPoint ControlPointPrefab;
        private bool _enabled;

        protected AbstractControlPointGenerator(IInputProvider inputProvider, ControlPointStore store,
            ControlPoint controlPointPrefab)
        {
            InputProvider = inputProvider;
            _store = store;
            ControlPointPrefab = controlPointPrefab;

            _enabled = true;
            GameManager.OnGameStateChanged += OnGameStateChanged;
        }

        ~AbstractControlPointGenerator()
        {
            Destroy();
        }

        public virtual void Destroy()
        {
            GameManager.OnGameStateChanged -= OnGameStateChanged;
            OnControlPointGenerated = null;
            _store = null;
        }

        public void Create(Vector3 position)
        {
            if (!_enabled) return;

            ControlPoint controlPoint = Generate(position);
            controlPoint.name = $"ControlPoint{_store.Count}";
            Utils.CustomDebugLog(Utils.LogLevel.Info, $"{controlPoint.name} generated at {position}");

            _store.AddControlPoint(controlPoint);
            OnControlPointGenerated?.Invoke(controlPoint);
        }

        protected abstract ControlPoint Generate(Vector3 position);

        private void OnGameStateChanged(GameManager.GameState gameState)
        {
            _enabled = gameState == GameManager.GameState.EditingRoad;
        }
    }
}