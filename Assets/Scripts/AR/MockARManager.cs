﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace AR
{
    public class MockARManager : IARManager
    {
        public void SetOcclusionActive(bool active)
        {
            Utils.CustomDebugLog(Utils.LogLevel.Info, $"MockARManager.SetOcclusionActive({active})");
        }

        public void SetPlaneDetectionActive(bool active)
        {
            Utils.CustomDebugLog(Utils.LogLevel.Info, $"MockARManager.SetPlaneDetectionActive({active})");
        }

        public bool Raycast(Vector2 screenPoint, List<ARRaycastHit> hitResults, TrackableType trackableType)
        {
            Utils.CustomDebugLog(Utils.LogLevel.Info, "MockARManager.Raycast");
            return false;
        }
    }
}