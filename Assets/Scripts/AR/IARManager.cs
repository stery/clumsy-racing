﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace AR
{
    public interface IARManager
    {
        public void SetOcclusionActive(bool active);
        public void SetPlaneDetectionActive(bool active);
        public bool Raycast(Vector2 screenPoint, List<ARRaycastHit> hitResults, TrackableType trackableType);
    }
}