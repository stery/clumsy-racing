﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace AR
{
    public class ARManager : IARManager
    {
        private readonly AROcclusionManager _arOcclusionManager;
        private readonly ARPlaneManager _arPlaneManager;
        private readonly ARRaycastManager _arRaycastManager;

        public ARManager([NotNull] AROcclusionManager arOcclusionManager,
            [NotNull] ARPlaneManager arPlaneManager,
            [NotNull] ARRaycastManager arRaycastManager)
        {
            _arOcclusionManager = arOcclusionManager;
            _arPlaneManager = arPlaneManager;
            _arRaycastManager = arRaycastManager;
        }

        public void SetOcclusionActive(bool active)
        {
            _arOcclusionManager.requestedEnvironmentDepthMode =
                active ? EnvironmentDepthMode.Medium : EnvironmentDepthMode.Disabled;
        }

        public void SetPlaneDetectionActive(bool active)
        {
            _arPlaneManager.requestedDetectionMode = active ? PlaneDetectionMode.Horizontal : PlaneDetectionMode.None;
            foreach (ARPlane plane in _arPlaneManager.trackables)
            {
                plane.gameObject.SetActive(active);
            }
        }

        public bool Raycast(Vector2 screenPoint, List<ARRaycastHit> hitResults, TrackableType trackableType)
        {
            return _arRaycastManager.Raycast(screenPoint, hitResults, trackableType);
        }
    }
}