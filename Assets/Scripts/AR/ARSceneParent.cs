﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace AR
{
    [RequireComponent(typeof(ARAnchor))]
    public sealed class ARSceneParent : SceneParent
    {
    }
}